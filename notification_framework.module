<?php

/**
 * @file
 * Module file.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\notification_framework\Hooks\EntityCrudHooks;
use Drupal\notification_framework\Hooks\MailHooks;

/**
 * Implements hook_theme().
 */
function notification_framework_theme() {
  return [
    'notification_framework_single_entity' => [
      'variables' => [
        'entity_label' => NULL,
        'change_summary' => NULL,
        'notify_entity_exists' => NULL,
        'notify_entity' => NULL,
        'operation' => NULL,
        'account' => NULL,
      ],
    ],
    'notification_framework_email_wrapper' => [
      'variables' => [
        'children' => NULL,
        'preferences_url' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_entity_update().
 */
function notification_framework_entity_update(EntityInterface $entity) {
  \Drupal::classResolver(EntityCrudHooks::class)->entityUpdate($entity);
}

/**
 * Implements hook_entity_insert().
 */
function notification_framework_entity_insert(EntityInterface $entity) {
  \Drupal::classResolver(EntityCrudHooks::class)->entityInsert($entity);
}

/**
 * Implements hook_entity_delete().
 */
function notification_framework_entity_delete(EntityInterface $entity) {
  \Drupal::classResolver(EntityCrudHooks::class)->entityDelete($entity);
}

/**
 * Implements hook_mail().
 */
function notification_framework_mail($key, &$message, $params) {
  if ($key === 'notification') {
    \Drupal::classResolver(MailHooks::class)->mail($key, $message, $params);
  }
}

/**
 * Implements hook_module_implements_alter().
 */
function notification_framework_module_implements_alter(&$implementations, $hook) {
  // Run our entity delete hook first, so we can operate while group_content
  // association entities still exist.
  if ($hook === 'entity_delete') {
    $group = $implementations['notification_framework'];
    unset($implementations['notification_framework']);
    $implementations = [
      'notification_framework' => $group
    ] + $implementations;
  }
}

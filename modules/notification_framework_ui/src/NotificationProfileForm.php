<?php

namespace Drupal\notification_framework_ui;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\ProfileForm;
use Drupal\user\UserInterface;

/**
 * A profile form for the notifications interface.
 */
class NotificationProfileForm extends ProfileForm {

  /**
   * A callback for the page title.
   */
  public static function titleCallback(UserInterface $user) {
    $current_user = \Drupal::currentUser();
    return $current_user->id() === $user->id() ? t('Notifications') : t('Notifications for :user', [':user' => $user->getDisplayName()]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    if (isset($form['role_change'])) {
      $form['role_change']['#access'] = FALSE;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    unset($element['delete']);
    return $element;
  }

}

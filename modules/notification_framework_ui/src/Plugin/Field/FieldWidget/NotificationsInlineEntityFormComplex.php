<?php

namespace Drupal\notification_framework_ui\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\inline_entity_form\Plugin\Field\FieldWidget\InlineEntityFormComplex;

/**
 * Notifications complex inline widget.
 *
 * @FieldWidget(
 *   id = "notifications_inline_entity_form_complex",
 *   label = @Translation("Notification framework inline entity form"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions",
 *   },
 *   multiple_values = true
 * )
 */
class NotificationsInlineEntityFormComplex extends InlineEntityFormComplex {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // If there is a single button, replace the label to be consistent.
    if (!empty($element['actions']['bundle']['#value'])) {
      $type = \Drupal::service('plugin.manager.notification_framework_type')->createInstance($element['actions']['bundle']['#value']);
      $element['actions']['ief_add']['#value'] = t('Add :bundle notification', [':bundle' => $type->getPluginDefinition()['label']]);
    }

    // If there is a select list of bundles, replace the bundle list select list
    // with a button for each bundle.
    if (!empty($element['actions']['bundle']['#options'])) {
      $bundles = $element['actions']['bundle']['#options'];
      foreach ($bundles as $bundle_id => $bundle_label) {
        $element['actions']['ief_add_' . $bundle_label] = $element['actions']['ief_add'];
        $element['actions']['ief_add_' . $bundle_label]['#value'] = t('Add :bundle notification', [':bundle' => $bundle_label]);
        $element['actions']['ief_add_' . $bundle_label]['#submit'] = [[static::class, 'openFormButtons']];
        $element['actions']['ief_add_' . $bundle_label]['#ief_bundle'] = $bundle_id;
      }

      // Hide the old button and select list.
      $element['actions']['bundle']['#access'] = FALSE;
      $element['actions']['ief_add']['#access'] = FALSE;
    }

    if ($this->anyEntityNeedsSave($form_state->get(['inline_entity_form', $element['#ief_id']]))) {
      $element['warning'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#weight' => -1,
        '#value' => $this->t('You have unsaved changes. You must click "Save" for changes to apply.'),
        '#attributes' => [
          'class' => ['messages', 'messages--warning'],
        ],
      ];
    }

    return $element;
  }

  /**
   * Check if any entity needs a save.
   */
  protected function anyEntityNeedsSave(array $widgetState): bool {
    $entities_need_save = count(array_filter(array_column($widgetState['entities'], 'needs_save'))) > 0;
    $entities_need_delete = count($widgetState['delete']) > 0;
    return $entities_need_save || $entities_need_delete;
  }

  /**
   * Button #submit callback: Opens a form in the IEF widget.
   */
  public static function openFormButtons($form, FormStateInterface $form_state) {
    $element = inline_entity_form_get_element($form, $form_state);
    $ief_id = $element['#ief_id'];
    $form_state->setRebuild();
    $triggering_element = $form_state->getTriggeringElement();
    $form_state->set(['inline_entity_form', $ief_id, 'form'], $triggering_element['#ief_form']);
    if (!empty($triggering_element['#ief_bundle'])) {
      $form_state->set(['inline_entity_form', $ief_id, 'form settings'], [
        'bundle' => $triggering_element['#ief_bundle'],
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return $field_definition->getName() === 'field_notifications' && $field_definition->getTargetEntityTypeId() === 'user';
  }

}

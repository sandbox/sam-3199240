<?php

namespace Drupal\notification_framework_enforce;

use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\user\UserInterface;

/**
 * Hook bridge for account updates.
 */
class UserUpdate extends EnforceHookBase {

  /**
   * Implements hook_ENTITY_TYPE_presave().
   */
  public function presave(UserInterface $account) {
    if ($this->skipEnforce) {
      return;
    }

    $originally_had_officer_access = isset($account->original) && $this->userEligibleForNotifications($account->original);
    $now_has_officer_access = !$account->isNew() && $this->userEligibleForNotifications($account);

    $default_officer_notification_types = [
      'officer_view',
      'document',
    ];

    // Create notifications if a user is being granted access.
    if (!$originally_had_officer_access && $now_has_officer_access) {
      foreach ($default_officer_notification_types as $default_officer_notification_type) {
        if (!$this->userHasNotificationType($account, $default_officer_notification_type)) {
          $notification = NotificationPreference::create([
            'type' => $default_officer_notification_type,
            'owner' => $account->id(),
          ]);
          $notification->save();
          $account->field_notifications[] = $notification->id();
        }
      }
    }

    // Remove notifications if the user is being blocked, for all types of
    // users.
    $originally_had_site_access = isset($account->original) && $account->original->isActive();
    $now_has_site_access = !$account->isNew() && $account->isActive();
    if ($originally_had_site_access && !$now_has_site_access) {
      foreach ($account->field_notifications as $notification_item) {
        $notification_item->entity->delete();
      }
      $account->field_notifications = NULL;
    }
  }

  /**
   * Check if a user is eligible for notifications.
   */
  protected function userEligibleForNotifications(UserInterface $account): bool {
    return $account->isActive() && $account->hasPermission('nf subscribe to notifications');
  }

  /**
   * Check if a user already has an officer view notification.
   */
  protected function userHasNotificationType(UserInterface $account, string $notificationType): bool {
    foreach ($account->field_notifications as $notification_field) {
      if ($notification_field->entity->bundle() === $notificationType) {
        return TRUE;
      }
    }
    return FALSE;
  }

}

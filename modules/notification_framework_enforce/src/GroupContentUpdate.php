<?php

namespace Drupal\notification_framework_enforce;

use Drupal\group\Entity\GroupContentInterface;
use Drupal\notification_framework\Entity\NotificationPreference;

/**
 * Hook bridge for group content.
 */
class GroupContentUpdate extends EnforceHookBase {

  /**
   * Implements hook_ENTITY_TYPE_insert() and hook_ENTITY_TYPE_update().
   */
  public function entityInsertUpdate(GroupContentInterface $entity) {
    if ($this->skipEnforce) {
      return;
    }

    if ($entity->getContentPlugin()->getPluginId() !== 'group_membership') {
      return;
    }

    // Skip creating default notifications for SCC users.
    if ($entity->getGroup()->getGroupType()->id() === 'scc') {
      return;
    }

    $membership_approved = $entity->group_requires_approval->value === 0;
    $original_membership_approved = isset($entity->original) && $entity->original->group_requires_approval->value === 0;

    // If the user is now a member of a group and they previously weren't, add
    // the notification to the users profile.
    if (!$original_membership_approved && $membership_approved) {
      $owner = $entity->getEntity();
      $group = $entity->getGroup();
      $notification = NotificationPreference::create([
        'type' => $group->getGroupType()->id(),
        'town_square_group' => $group->id(),
        'owner' => $owner->id(),
      ]);
      $notification->save();

      $owner->field_notifications[] = $notification->id();
      $owner->save();
    }
  }

}

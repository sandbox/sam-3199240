<?php

namespace Drupal\notification_framework_enforce;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface;
use Drupal\notification_framework\PluginManager\NotificationTypeManager;
use Drupal\user\UserInterface;

/**
 * Enforce notifications on user profiles.
 */
class BatchEnforcer {

  /**
   * The notification type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationTypeManager
   */
  protected $notificationTypeManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * NotificationPreferenceEnforcer constructor.
   */
  public function __construct(NotificationTypeManager $notificationTypeManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->notificationTypeManager = $notificationTypeManager;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Get the notifications applicable for the given user.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account.
   * @param array $types
   *   Type of notifications to limit the search for.
   *
   * @return \Drupal\notification_framework\Entity\NotificationPreference[]
   *   An unsaved list of notification preference entities.
   */
  public function getApplicableNotifications(UserInterface $account, array $types = []): array {
    $notifications = [];

    $notification_type_ids = !empty($types) ? $types : array_map(function ($definition) {
      return $definition['id'];
    }, $this->notificationTypeManager->getDefinitions());

    $access_handler = $this->entityTypeManager->getAccessControlHandler('notification_framework_preference');
    foreach ($notification_type_ids as $notification_type_id) {
      $type_plugin = $this->notificationTypeManager->createInstance($notification_type_id);

      if ($access_handler->createAccess($notification_type_id, $account)) {
        $notifications = array_merge($notifications, $this->createDefaultPreferences($account, $type_plugin));
      }
    }

    return $notifications;
  }

  /**
   * Create the default preferences for a user and type.
   *
   * Do not include this as part of the plugins signature, because this may be
   * a one off process. For group and officer based notifications, they will
   * need to be generated as membership to groups and the officer role is
   * granted, so this service will not need to run regularly, only to
   * retroactively address the existing pool of users.
   */
  protected function createDefaultPreferences(UserInterface $user, NotificationFrameworkTypeInterface $type): array {
    $preferences = [];

    $preferences[] = NotificationPreference::create([
      'type' => $type->getPluginId(),
      'owner' => $user->id(),
    ]);

    return $preferences;
  }

  /**
   * Attach some unsaved notification preferences to a user account.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   * @param \Drupal\notification_framework\Entity\NotificationPreference[] $preferences
   *   An unsaved list of preferences.
   */
  public function attachUnsavedPreferencesToUser(UserInterface $account, array $preferences) {
    $field = [];
    foreach ($preferences as $preference) {
      $preference->save();
      $field[] = $preference->id();
    }
    // Intentionally squash existing notifications, since they should not exist
    // at all.
    $account->field_notifications = $field;
    $account->save();
  }

  /**
   * Merged some unsaved notification preferences to a user account.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   * @param \Drupal\notification_framework\Entity\NotificationPreference[] $preferences
   *   An unsaved list of preferences.
   */
  public function mergeUnsavedPreferencesToUser(UserInterface $account, array $preferences) {
    foreach ($preferences as $preference) {
      $preference->save();
      $account->field_notifications[] = $preference->id();
    }
    $account->save();
  }

}

<?php

namespace Drupal\notification_framework_enforce;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\notification_framework\PluginManager\NotificationTypeManager;
use \Drupal\notification_framework\ChunkedIterator;
use Drupal\user\UserInterface;

/**
 * The NotificationUserUpdate class.
 */
class BatchUserUpdate {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The enforcer.
   *
   * @var \Drupal\notification_framework_enforce\BatchEnforcer
   */
  protected $enforcer;

  /**
   * The entity memory cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $memoryCache;

  /**
   * The notification type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationTypeManager
   */
  protected $notificationTypeManager;

  /**
   * NotificationUserUpdate constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, BatchEnforcer $enforcer, MemoryCacheInterface $memoryCache, NotificationTypeManager $notificationTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->enforcer = $enforcer;
    $this->memoryCache = $memoryCache;
    $this->notificationTypeManager = $notificationTypeManager;
  }

  /**
   * Assign notifications to all users.
   */
  public function assignToAllUsers() {
    $this->iterateActiveUsers(function (UserInterface $account) {
      $preferences = $this->enforcer->getApplicableNotifications($account);
      $this->enforcer->attachUnsavedPreferencesToUser($account, $preferences);
    });
  }

  /**
   * Iterate the active users on the site.
   */
  protected function iterateActiveUsers(callable $fn) {
    $user_storage = $this->entityTypeManager->getStorage('user');
    $query = $user_storage->getQuery();
    $query->condition('status', 1);
    $user_iterator = new ChunkedIterator($user_storage, $this->memoryCache, $query->execute());
    $updates = 0;
    foreach ($user_iterator->getIterator() as $account) {
      $fn($account);
      $updates++;
      print sprintf("Completed %d/%d user updates.\n", $updates, $user_iterator->count());
    }
  }

}

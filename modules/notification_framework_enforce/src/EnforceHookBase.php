<?php

namespace Drupal\notification_framework_enforce;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for hooks that enforce notifications.
 */
class EnforceHookBase implements ContainerInjectionInterface {

  /**
   * If the enforce process should be skipped.
   *
   * @var bool
   */
  protected $skipEnforce;

  /**
   * UpdateHookBase constructor.
   */
  public function __construct(bool $skip) {
    $this->skipEnforce = $skip;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('state')->get('notification_framework_enforce_skip', FALSE));
  }

}

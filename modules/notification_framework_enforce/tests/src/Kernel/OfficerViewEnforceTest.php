<?php

namespace Drupal\Tests\notification_framework_enforce\Functional;

use Drupal\Tests\notification_framework\Kernel\NotificationFrameworkKernelTestBase;

/**
 * @coversDefaultClass \Drupal\notification_framework_enforce\UserUpdate
 */
class OfficerViewEnforceTest extends NotificationFrameworkKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->container->get('state')->set('notification_framework_enforce_skip', FALSE);
  }

  /**
   * @covers ::presave
   */
  public function testOfficerViewEnforce() {
    // Test adding an officer directly adds the notifications.
    $officer = $this->createOfficer();
    $this->assertEquals(2, $officer->field_notifications->count());

    // Blocking and activating a user shouldn't add an additional notification.
    $officer->block();
    $officer->save();
    $officer->activate();
    $officer->save();
    $this->assertEquals(2, $officer->field_notifications->count());

    // Adding the officer role should create the notification.
    $account = $this->createUser();
    $this->assertEquals(0, $account->field_notifications->count());
    $account->addRole('officer');
    $account->save();
    $this->assertEquals(2, $account->field_notifications->count());

    // Creating a user with the officer role, but blocked should also create
    // the notification once activated.
    $blocked_officer = $this->createUserWithRoleAndFieldValues('officer', [
      'status' => 0,
    ]);
    $this->assertEquals(0, $blocked_officer->field_notifications->count());
    $blocked_officer->activate();
    $blocked_officer->save();
    $this->assertEquals(2, $blocked_officer->field_notifications->count());
  }

  /**
   * @covers ::presave
   */
  public function testRoleDelegationOfficerEnforce() {
    $account = $this->createUser();
    $account->field_profession = 7;
    $account->save();
    $this->assertEquals(0, $account->field_notifications->count());

    $this->drupalLogin($this->createToolboxAdmin());

    $this->drupalPostForm($account->toUrl('edit-form'), [
      'role_change[editor]' => TRUE,
    ], 'Save');

    $account = $this->reloadEntity($account);
    $this->assertEquals(2, $account->field_notifications->count());
  }

  /**
   * Test the type of notifications enforced.
   */
  public function testOfficerViewEnforceTypes() {
    $officer = $this->createOfficer();
    $types = [];
    foreach ($officer->field_notifications as $notification_item) {
      $types[] = $notification_item->entity->bundle();
    }
    $this->assertEquals(['officer_view', 'document'], $types);
  }

}

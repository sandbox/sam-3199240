<?php

namespace Drupal\Tests\notification_framework_enforce\Functional;

use Drupal\Tests\notification_framework\Kernel\NotificationFrameworkKernelTestBase;

/**
 * @coversDefaultClass \Drupal\notification_framework_enforce\GroupContentUpdate
 */
class GroupMembershipEnforceTest extends NotificationFrameworkKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->container->get('state')->set('notification_framework_enforce_skip', FALSE);
  }

  /**
   * @covers ::entityInsertUpdate
   * @dataProvider groupMembershipEnforceTestCases
   */
  public function testGroupMembershipEnforce($group_type, $is_given_notifications) {
    $group = $this->createGroup($group_type);
    $account = $this->createUser();

    // The user will not start with notifications.
    $this->assertEquals(0, $account->field_notifications->count());

    // Adding a membership without approval will not add a notification.
    $group->addMember($account, [
      'group_roles' => [$group->getGroupType()->id() . '-member'],
      'group_requires_approval' => 1,
    ]);
    $this->assertEquals(0, $account->field_notifications->count());

    // Approving the membership will add the noticiation.
    $group_content = $group->getMember($account)->getGroupContent();
    $this->assertEquals(1, $group_content->group_requires_approval->value);

    $group_content->group_requires_approval = 0;
    $group_content->save();

    $account = $this->reloadEntity($account);
    if ($is_given_notifications) {
      $this->assertEquals(1, $account->field_notifications->count());
      $this->assertEquals($group_type, $account->field_notifications[0]->entity->bundle());
    }
    else {
      $this->assertTrue($account->field_notifications->isEmpty());
    }
  }

  /**
   * Test cases for ::testGroupMembershipEnforce.
   */
  public function groupMembershipEnforceTestCases() {
    return [
      [
        'town_square',
        TRUE,
      ],
      [
        'scc',
        FALSE,
      ],
    ];
  }

}

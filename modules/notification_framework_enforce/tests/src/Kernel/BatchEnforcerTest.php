<?php

namespace Drupal\Tests\notification_framework_enforce\Functional;

use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\Tests\notification_framework\Kernel\NotificationFrameworkKernelTestBase;
use Drupal\user\UserInterface;

/**
 * @coversDefaultClass \Drupal\notification_framework_enforce\BatchEnforcer
 */
class BatchEnforcerTest extends NotificationFrameworkKernelTestBase {

  /**
   * The enforcer.
   *
   * @var \Drupal\notification_framework_enforce\BatchEnforcer
   */
  protected $enforcer;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->enforcer = $this->container->get('notification_framework_enforce.batch_enforcer');
  }

  /**
   * @covers ::getApplicableNotifications
   */
  public function testGetApplicableNotificationsOfficer() {
    $officer = $this->createOfficer();
    $this->assertNotificationsMatch([
      [
        'type' => 'document',
        'operations' => [
          'created',
          'updated',
        ],
        'participation' => 'all',
      ],
      [
        'type' => 'officer_view',
        'operations' => [
          'created',
          'commented',
        ],
        'participation' => 'all',
        'officer_view_types' => [
          'discussion',
          'officer_page',
          'project',
          'toolbox_tv_video',
          'vacancy',
        ],
      ],
    ], $officer);
  }

  /**
   * @covers ::getApplicableNotifications
   */
  public function testGetApplicableNotificationsGroupMember() {
    // Create a group member who is part of two two squares and a sunshine
    // coast group.
    $town_square = $this->createTownSquare('Foosquare');
    $member = $this->createGroupMember($town_square);
    $sunshine_coast_group = $this->createGroup('scc', 'Foocoastgroup');
    $sunshine_coast_group->addMember($member, [
      'group_roles' => [$town_square->getGroupType()->id() . '-member'],
    ]);
    $second_town_square = $this->createTownSquare('Barsquare');
    $second_town_square->addMember($member, [
      'group_roles' => [$second_town_square->getGroupType()->id() . '-member'],
    ]);

    $this->assertNotificationsMatch([
      [
        'type' => 'town_square',
        'operations' => [
          'created',
          'commented',
        ],
        'participation' => 'all',
        'town_square_group' => $town_square->id(),
        'town_square_types' => [
          'planned_outage',
          'key_decisions',
          'generic_register',
          'page',
          'announcement',
          'incident_register',
          'document',
          'meeting',
          'discussion',
        ],
      ],
      [
        'type' => 'town_square',
        'operations' => [
          'created',
          'commented',
        ],
        'participation' => 'all',
        'town_square_group' => $second_town_square->id(),
        'town_square_types' => [
          'planned_outage',
          'key_decisions',
          'generic_register',
          'page',
          'announcement',
          'incident_register',
          'document',
          'meeting',
          'discussion',
        ],
      ],
    ], $member);
  }

  /**
   * @covers ::getApplicableNotifications
   */
  public function testGetApplicableNotificationsOfficerAndGroupMember() {
    $officer_and_group_member = $this->createOfficer();
    $town_square = $this->createTownSquare('Barsquare');
    $town_square->addMember($officer_and_group_member, [
      'group_roles' => [$town_square->getGroupType()->id() . '-member'],
    ]);

    $this->assertNotificationsMatch([
      [
        'type' => 'document',
        'operations' => [
          'created',
          'updated',
        ],
        'participation' => 'all',
      ],
      [
        'type' => 'officer_view',
        'operations' => [
          'created',
          'commented',
        ],
        'participation' => 'all',
        'officer_view_types' => [
          'discussion',
          'officer_page',
          'project',
          'toolbox_tv_video',
          'vacancy',
        ],
      ],
      [
        'type' => 'town_square',
        'operations' => [
          'created',
          'commented',
        ],
        'participation' => 'all',
        'town_square_group' => $town_square->id(),
        'town_square_types' => [
          'planned_outage',
          'key_decisions',
          'generic_register',
          'page',
          'announcement',
          'incident_register',
          'document',
          'meeting',
          'discussion',
        ],
      ],
    ], $officer_and_group_member);
  }

  /**
   * @covers ::getApplicableNotifications
   */
  public function testGetApplicableNotificationsOfficerAndGroupMemberLimitedIds() {
    $officer_and_group_member = $this->createOfficer();
    $town_square = $this->createTownSquare('Barsquare');
    $town_square->addMember($officer_and_group_member, [
      'group_roles' => [$town_square->getGroupType()->id() . '-member'],
    ]);

    $this->assertNotificationsMatch([
      [
        'type' => 'document',
        'operations' => [
          'created',
          'updated',
        ],
        'participation' => 'all',
      ],
    ], $officer_and_group_member, ['document']);
  }

  /**
   * @covers ::attachUnsavedPreferencesToUser
   */
  public function testAttachUnsavedPreferencesToUser() {
    $officer = $this->createOfficer();
    $this->assertEquals(0, $officer->field_notifications->count());
    $notifications = $this->enforcer->getApplicableNotifications($officer);
    $this->enforcer->attachUnsavedPreferencesToUser($officer, $notifications);
    $this->assertEquals(2, $officer->field_notifications->count());
  }

  /**
   * @covers ::mergeUnsavedPreferencesToUser
   */
  public function testMergeUnsavedPreferencesToUser() {
    $officer = $this->createOfficer();
    $this->createNotificationPreference($officer, [
      'type' => 'document',
    ]);
    $this->assertEquals(1, $officer->field_notifications->count());

    $notifications = [
      NotificationPreference::create([
        'owner' => $officer->id(),
        'type' => 'document',
      ]),
    ];
    $this->markEntityForCleanup($notifications[0]);

    $this->enforcer->mergeUnsavedPreferencesToUser($officer, $notifications);
    $this->assertEquals(2, $officer->field_notifications->count());
  }

  /**
   * Assert a group of notifications match an expected structure.
   */
  protected function assertNotificationsMatch(array $expected, UserInterface $owner, $ids = []) {
    $notifications = $this->enforcer->getApplicableNotifications($owner, $ids);

    // Turn notification entities into an array of fields we care about.
    $snapshot = array_map(function (NotificationPreference $notification) use ($owner) {
      $this->assertEquals($owner->id(), $notification->getOwnerId());
      return array_filter($notification->toArray(), function ($key) {
        return in_array($key, [
          'officer_view_types',
          'operations',
          'participation',
          'officer_view_types',
          'town_square_group',
          'town_square_types',
          'type',
        ]);
      }, ARRAY_FILTER_USE_KEY);
    }, $notifications);
    // Squash the entity into a flatter structure.
    $snapshot = array_map(function ($entity) {
      return array_map(function ($field_value) {
        $field_value = array_column($field_value, array_key_first($field_value[0]));
        return count($field_value) === 1 ? $field_value[0] : $field_value;
      }, $entity);
    }, $snapshot);

    $this->assertSame($this->sortArrayByValue($expected), $this->sortArrayByValue($snapshot));
  }

  /**
   * Sort an array by it's values at a single level of depth.
   *
   * @param array $array
   *   An array to sort.
   *
   * @return array
   *   A sorted array.
   */
  protected function sortArrayByValue(array $array): array {
    $new_array = [];
    foreach ($array as $element) {
      $new_array[md5(serialize($element))] = $element;
    }
    ksort($new_array);
    return $new_array;
  }

}

<?php

namespace Drupal\Tests\notification_framework_enforce\Functional;

use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\Tests\notification_framework\Kernel\NotificationFrameworkKernelTestBase;

/**
 * Test blocking user unenforces notifications.
 */
class BlockedUserUnenforceTest extends NotificationFrameworkKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->container->get('state')->set('notification_framework_enforce_skip', FALSE);
  }

  /**
   * Test blocking a user removes their notifications.
   */
  public function testBlockedOfficerNotifications() {
    $officer = $this->createOfficer();
    $this->assertEquals(2, $officer->field_notifications->count());

    $ids = array_column($officer->field_notifications->getValue(), 'target_id');
    $this->assertCount(2, $ids);
    $this->assertCount(2, NotificationPreference::loadMultiple($ids));

    $officer->block();
    $officer->save();

    $this->assertEquals(0, $officer->field_notifications->count());
    $this->assertEmpty(NotificationPreference::loadMultiple($ids));
  }

  /**
   * Test blocking a user removes their notifications.
   */
  public function testBlockedTsqNotifications() {
    $group = $this->createTownSquare();
    $member = $this->createUser();

    $this->assertEquals(0, $member->field_notifications->count());

    $group->addMember($member, [
      'group_roles' => [$group->getGroupType()->id() . '-member'],
      'group_requires_approval' => 1,
    ]);
    $group_content = $group->getMember($member)->getGroupContent();
    $group_content->group_requires_approval = 0;
    $group_content->save();

    $member = $this->reloadEntity($member);
    $this->assertEquals(1, $member->field_notifications->count());

    $ids = array_column($member->field_notifications->getValue(), 'target_id');
    $this->assertCount(1, $ids);
    $this->assertCount(1, NotificationPreference::loadMultiple($ids));

    $member->block();
    $member->save();

    $this->assertEquals(0, $member->field_notifications->count());
    $this->assertEmpty(NotificationPreference::loadMultiple($ids));
  }

}

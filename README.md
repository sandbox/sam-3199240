Notification framework
===

Notifications workflow:

1. Content CRUD happens and is queued with a serialized version of the entity.
2. Queue worker matches content to the notification preferences of the site.
3. Users access to the content is validated and corresponding "Notification"
   entities are created to capture all metadata required to send an email or
   present a summary of the event. No content other than that captured in the
   Notification entity is required to notify users. In some cases it might be
   helpful to generate a link to a live piece of content, but it should be
   optional in the cases where that content no longer exists (such as in the
   case of deleted content).
4. Notification entities are filtered through a series of formatter plugins
   which decide the presentation of a collection of Notification entities.
5. Emails are sent to the user.

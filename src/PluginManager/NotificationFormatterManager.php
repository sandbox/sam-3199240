<?php

namespace Drupal\notification_framework\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatterInterface;

/**
 * The NotificationTypeManager class.
 */
class NotificationFormatterManager extends DefaultPluginManager {

  /**
   * Constructs NotificationTypeManager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/NotificationFramework/NotificationFrameworkFormatter', $namespaces, $module_handler, '\Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatterInterface', '\Drupal\notification_framework\Annotation\NotificationFrameworkFormatter');
    $this->alterInfo('notification_framework_formatter_plugins');
    $this->setCacheBackend($cache_backend, 'notification_framework_formatter_plugins');
  }

  /**
   * Create a formatter plugin to handle a collection of notifications.
   */
  public function createInstanceForNotifications(array $notifications): ?NotificationFrameworkFormatterInterface {
    foreach ($this->getDefinitions() as $definition) {
      /** @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatterInterface $instance */
      $instance = $this->createInstance($definition['id'], []);
      if ($instance->isApplicable($notifications)) {
        return $instance;
      }
    }
    return NULL;
  }

}

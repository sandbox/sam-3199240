<?php

namespace Drupal\notification_framework\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface;

/**
 * The NotificationTypeManager class.
 */
class NotificationTypeManager extends DefaultPluginManager {

  /**
   * Constructs NotificationTypeManager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/NotificationFramework/NotificationFrameworkType', $namespaces, $module_handler, '\Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface', '\Drupal\notification_framework\Annotation\NotificationFrameworkType');
    $this->alterInfo('notification_framework_type_plugins');
    $this->setCacheBackend($cache_backend, 'notification_framework_type_plugins');
  }

  /**
   * Create a type plugin to handle the given entity.
   */
  public function createInstanceForEntity(EntityInterface $entity, array $configuration = []): ?NotificationFrameworkTypeInterface {
    foreach ($this->getDefinitions() as $definition) {
      /** @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface $instance */
      $instance = $this->createInstance($definition['id'], $configuration);
      if ($instance->isApplicable($entity)) {
        return $instance;
      }
    }
    return NULL;
  }

}

<?php

namespace Drupal\notification_framework\PluginManager;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummaryInterface;

/**
 * The NotificationTypeManager class.
 */
class NotificationSummaryManager extends DefaultPluginManager {

  /**
   * Constructs NotificationTypeManager.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/NotificationFramework/NotificationFrameworkSummary', $namespaces, $module_handler, '\Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummaryInterface', '\Drupal\notification_framework\Annotation\NotificationFrameworkSummary');
    $this->alterInfo('nf_notification_summary_plugins');
    $this->setCacheBackend($cache_backend, 'nf_notification_summary_plugins');
  }

  /**
   * Create a summary plugin to handle an entity.
   */
  public function createInstanceForEntity(EntityInterface $entity, $operation): ?NotificationFrameworkSummaryInterface {
    foreach ($this->getDefinitions() as $definition) {
      /** @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummaryInterface $instance */
      $instance = $this->createInstance($definition['id'], []);
      if ($instance->isApplicable($entity, $operation)) {
        return $instance;
      }
    }
    return NULL;
  }

}

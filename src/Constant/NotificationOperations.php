<?php

namespace Drupal\notification_framework\Constant;

/**
 * Notification operations enum.
 */
class NotificationOperations {

  /**
   * Operation for content being created.
   */
  const CREATED = 'created';

  /**
   * Operation for content being updated.
   */
  const UPDATED = 'updated';

  /**
   * Operation for content being commented.
   */
  const COMMENTED = 'commented';

  /**
   * Operation for content being deleted.
   */
  const DELETED = 'deleted';

}

<?php

namespace Drupal\notification_framework\Constant;

/**
 * Notification participation enum.
 */
class NotificationParticipation {

  /**
   * All types of content.
   */
  const ALL = 'all';

  /**
   * The user participated in content by creating it.
   */
  const CREATED = 'created';

  /**
   * The user participated in content by commenting on it.
   */
  const COMMENTED = 'commented';

}

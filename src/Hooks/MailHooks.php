<?php

namespace Drupal\notification_framework\Hooks;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\notification_framework\Entity\Notification;
use Drupal\notification_framework\PluginManager\NotificationFormatterManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hooks bridge for mail hooks.
 */
class MailHooks implements ContainerInjectionInterface {

  /**
   * The formatter manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationFormatterManager
   */
  protected $notificationFormatterManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * MailHooks constructor.
   */
  public function __construct(NotificationFormatterManager $notificationFormatterManager, RendererInterface $renderer) {
    $this->notificationFormatterManager = $notificationFormatterManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.notification_framework_formatter'),
      $container->get('renderer')
    );
  }

  /**
   * Implements hook_mail().
   */
  public function mail($key, &$message, $params) {
    /** @var \Drupal\notification_framework\Entity\Notification $notification */
    $notification = Notification::load($params['notification']);

    // Right now only single notifications are sent, but this could later be
    // expanded to format a group of various types of notification in a digest.
    // Any implementation trying to mail notifications would need to gather all
    // applicable notifications first and call the mailer with an array of
    // notification entities.
    $formatter = $this->notificationFormatterManager->createInstanceForNotifications([$notification]);

    $body = $formatter->getBody([$notification]);
    $message['body'][] = $this->renderer->renderPlain($body);

    $message['subject'] = $formatter->getSubject([$notification]);
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';

    $attachments = $formatter->getAttachments([$notification]);
    if (!empty($attachments)) {
      $message['params']['attachments'] = $attachments;
    }
  }

}

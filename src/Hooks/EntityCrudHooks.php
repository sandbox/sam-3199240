<?php

namespace Drupal\notification_framework\Hooks;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\SynchronizableInterface;
use Drupal\Core\Queue\QueueInterface;
use Drupal\notification_framework\EntityCrudQueueItem;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\notification_framework\NotificationSkippableInterface;
use Drupal\notification_framework\PluginManager\NotificationTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The EntityCrudHooks class.
 */
class EntityCrudHooks implements ContainerInjectionInterface {

  /**
   * A list of entity type that require association with a group.
   *
   * For the purposes of notifications, some entity types require association
   * with a group. When these entity types are created, they are saved as their
   * own stand alone item of content and then a group_content entity is created
   * to associate their membership to the group. For these entity types, instead
   * of responding to the insert of the actual entity we care about, we must
   * wait for the group_content entity to be inserted, so that as the entity
   * is serialized and queued to be matched up with preferences, the group
   * association can be queried and stored.
   *
   * @var array
   */
  protected $requiresGroupEntityTypes = [
    'town_square_content',
  ];

  /**
   * The notification crud queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $notificationCrudQueue;

  /**
   * The notification type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationTypeManager
   */
  protected $notificationTypeManager;

  /**
   * A list of recently created group entities.
   *
   * @var array
   */
  protected static $recentlyCreatedGroupEntities;

  /**
   * EntityCrudHooks constructor.
   */
  public function __construct(QueueInterface $queue, NotificationTypeManager $notificationTypeManager) {
    $this->notificationCrudQueue = $queue;
    $this->notificationTypeManager = $notificationTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue')->get('nf_notification_crud'),
      $container->get('plugin.manager.notification_framework_type')
    );
  }

  /**
   * Implements hook_entity_update().
   */
  public function entityUpdate(EntityInterface $entity): void {
    // Skip the update operation from group entities that have recently been
    // created.
    if (in_array($entity->getEntityTypeId(), $this->requiresGroupEntityTypes, TRUE) && isset(static::$recentlyCreatedGroupEntities[$entity->id()])) {
      unset(static::$recentlyCreatedGroupEntities[$entity->id()]);
      return;
    }

    $this->handleEntityOp($entity, NotificationOperations::UPDATED);
  }

  /**
   * Implements hook_entity_insert().
   */
  public function entityInsert(EntityInterface $entity): void {
    // Transform the insert of a comment into a 'commented' operation on the
    // commented entity.
    if ($entity->getEntityTypeId() === 'comment') {
      $commented_entity = $entity->getCommentedEntity();
      $commented_entity->_notificationComment = $entity;
      $this->handleEntityOp($commented_entity, NotificationOperations::COMMENTED);
      return;
    }

    // Skip entity types that belong to groups.
    if (in_array($entity->getEntityTypeId(), $this->requiresGroupEntityTypes, TRUE)) {
      // Store recently created group entities, so their associated update
      // operation forced by the group module can be ignored.
      static::$recentlyCreatedGroupEntities[$entity->id()] = $entity->id();
      return;
    }

    // Once group content has been created, load the original entity that was
    // saved and continue with the notification operations.
    if ($entity->getEntityTypeId() === 'group_content') {
      $group_content_entity = $entity->getEntity();
      if ($group_content_entity && in_array($group_content_entity->getEntityTypeId(), $this->requiresGroupEntityTypes, TRUE)) {
        $entity = $group_content_entity;
      }
    }

    $this->handleEntityOp($entity, NotificationOperations::CREATED);
  }

  /**
   * Implements hook_entity_delete().
   */
  public function entityDelete(EntityInterface $entity): void {
    // When users are deleted, make sure to clean up associated notification
    // preference entities.
    if ($entity->getEntityTypeId() === 'user') {
      foreach ($entity->field_notifications as $notification_item) {
        $notification_item->entity->delete();
      }
    }

    $this->handleEntityOp($entity, NotificationOperations::DELETED);
  }

  /**
   * Create notifications for a given entity and operation.
   */
  protected function handleEntityOp(EntityInterface $entity, string $operation): void {
    // Skip queuing notifications for any entity that is syncing. This isn't
    // used in the notification system but is potentially useful for future
    // update hooks that need to make programmatic updates to entities.
    if ($entity instanceof SynchronizableInterface && $entity->isSyncing()) {
      return;
    }
    // Allow custom entities implementing a special interface to skip
    // notifications for specific types of updates.
    if ($entity instanceof NotificationSkippableInterface && $entity->isNotificationsSkipped()) {
      return;
    }
    // If no type plugin deals with the given entity, no need to process it
    // further.
    if (!$type_plugin = $this->notificationTypeManager->createInstanceForEntity($entity)) {
      return;
    }
    // Queue the entity and operation to be processed later in the background,
    // since matching a CRUD operation to a group of preferences may be an
    // expensive operation.
    $this->notificationCrudQueue->createItem(EntityCrudQueueItem::create($entity, $operation));
  }

}

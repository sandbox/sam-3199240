<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;

/**
 * Interface for the formatters of notification emails.
 */
interface NotificationFrameworkSummaryInterface extends PluginInspectionInterface {

  /**
   * Return an HTML string summary of the changes made to an entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $operation
   *   The operation.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   An HTML summary of the changes.
   */
  public function getSummary(EntityInterface $entity, string $operation): FormattableMarkup;

  /**
   * Check if a plugin is applicable to format an entity and operation.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $operation
   *   The operation.
   *
   * @return bool
   *   TRUE if the plugin is applicable, FALSE otherwise.
   */
  public function isApplicable(EntityInterface $entity, string $operation): bool;

  /**
   * Get attachments that should be included in the email.
   *
   * Attachments should be an array of individual attachments following the
   * format expected by hook_mail():
   *   - An attachment with inline content:
   *     @code
   *       [
   *         'filecontent' => 'foo',
   *         'filename' => 'sample.txt',
   *         'filemime' => 'text/plan'
   *       ]
   *     @endcode
   *   - An attachment pointing to a file on disk:
   *     @code
   *       [
   *         'filepath' => 'private://filesystem-sample.txt',
   *         'filename' => 'filesystem-sample.txt',
   *         'filemime' => 'text/plan',
   *       ]
   *     @endcode
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   * @param string $operation
   *   The operation.
   *
   * @return array
   *   An array of attachments.
   */
  public function getAttachments(EntityInterface $entity, string $operation): array;

}

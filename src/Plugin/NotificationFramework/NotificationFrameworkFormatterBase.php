<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Url;

/**
 * Base class for notification type plugins.
 */
abstract class NotificationFrameworkFormatterBase extends PluginBase implements NotificationFrameworkFormatterInterface {

  /**
   * Prepare a URL for sending in an email.
   *
   * @param string $route_name
   *   The route name.
   * @param array $params
   *   (Optional) The route params.
   * @param array $query
   *   (Optional) The query params.
   *
   * @return string
   *   The URL.
   */
  protected function prepareUrl(string $route_name, array $params = [], array $query = []): string {
    return (string) (new Url($route_name, $params, ['query' => $query]))
      ->setAbsolute()
      ->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getBody(array $notifications): array {
    $notification = current($notifications);
    $build = [
      '#theme_wrappers' => [
        'notification_framework_email_wrapper' => [
          '#preferences_url' => $this->prepareUrl('notification_framework_ui.notifications', [
            'user' => $notification->getOwner()->id(),
          ]),
        ],
      ],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttachments(array $notifications): array {
    $attachments = [];
    /** @var \Drupal\notification_framework\Entity\Notification $notification */
    foreach ($notifications as $notification) {
      $attachments = array_merge($attachments, $notification->getSummary()->getAttachments());
    }
    return $attachments;
  }

}

<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\BundlePlugin\BundlePluginInterface;
use Drupal\notification_framework\Entity\NotificationPreference;

/**
 * Interface for the types of notifications that can be created.
 */
interface NotificationFrameworkTypeInterface extends BundlePluginInterface {

  /**
   * Does the notification type support content of the given type.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content that has triggered a notification.
   *
   * @return bool
   *   TRUE if it does, FALSE otherwise.
   */
  public function isApplicable(EntityInterface $entity) : bool;

  /**
   * Build an entity query for finding applicable notification preferences.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   A query to add conditions to.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The content that notifications are being built for.
   * @param string $op
   *   The operation that triggered the notification.
   *
   * @throws \Drupal\notification_framework\Exception\UnresolvablePreferenceMatchException
   *   Thrown when preference matching should not continue.
   */
  public function buildApplicableNotificationPreferencesEntityQuery(QueryInterface $query, EntityInterface $entity, string $op) : void;

  /**
   * Check if the content should create a notification for the given preference.
   *
   * @param \Drupal\Core\Entity\EntityInterface $content
   *   The content that has been created.
   * @param string $op
   *   The operation that triggered the notification.
   * @param \Drupal\notification_framework\Entity\NotificationPreference $notificationPreference
   *   The notification preference.
   *
   * @return bool
   *   TRUE if a notification should sent, FALSE otherwise.
   *
   * @throws \Drupal\notification_framework\Exception\UnresolvablePreferenceMatchException
   *   Thrown when preference matching should not continue.
   */
  public function shouldNotify(EntityInterface $content, string $op, NotificationPreference $notificationPreference): bool;

  /**
   * Summarise the settings attached to the given notification entity.
   *
   * @param \Drupal\notification_framework\Entity\NotificationPreference $entity
   *   The entity.
   *
   * @return string
   *   A summary of the chosen settings.
   */
  public function summarizeNotificationSettings(NotificationPreference $entity): string;

  /**
   * Check the create access for the given bundle.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function checkCreateAccess(AccountInterface $account) : AccessResultInterface;

  /**
   * Get metadata to store with a notification entity.
   *
   * This metadata might be helpful to the formatting of emails or any other
   * information that should be stored with the record of a notification. This
   * is a free-form schema that is serialized and stored with the notification.
   *
   * @param \Drupal\Core\Entity\EntityInterface $notifyEntity
   *   The entity being notified.
   * @param string $operation
   *   The operation.
   *
   * @return array
   *   An array of data to store with the notification.
   */
  public function getNotificationMetadata(EntityInterface $notifyEntity, string $operation) : array;

}

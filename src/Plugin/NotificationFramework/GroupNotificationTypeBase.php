<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\lgt_group\GroupMembershipInformation;
use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\notification_framework\Exception\UnresolvablePreferenceMatchException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A base class for group notification types.
 */
abstract class GroupNotificationTypeBase extends NotificationFrameworkTypeBase implements ContainerFactoryPluginInterface, GroupNotificationTypeInterface {

  /**
   * The membership information service.
   *
   * @var \Drupal\lgt_group\GroupMembershipInformation
   */
  protected $membershipInformation;

  /**
   * TownSquareNotification constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, GroupMembershipInformation $memberInformation) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->membershipInformation = $memberInformation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('lgt_group.membership_information')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['town_square_group'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Group'))
      ->setSetting('target_type', 'group')
      ->setRequired(TRUE)
      ->setCardinality(1)
      ->setSetting('handler', 'group:membership_town_squares')
      ->setSetting('handler_settings', [])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 1,
        'settings' => [],
      ]);

    $fields['town_square_types'] = BundleFieldDefinition::create('list_string')
      ->setLabel(t('Content types'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => $this->getTownSquareContentTypes(),
      ])
      ->setDefaultValue(array_keys($this->getTownSquareContentTypes()))
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 2,
        'settings' => [],
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function checkCreateAccess(AccountInterface $account): AccessResultInterface {
    // Only allow users to create town square notifications if they are a
    // member of a town square group.
    $town_square_groups = $this->membershipInformation->loadGroupsByApprovedMembership($account, $this->getGroupType());
    return AccessResult::allowedIf(count($town_square_groups) > 0)->addCacheTags(['group_content_list:plugin:group_membership']);
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity): bool {
    if ($entity->getEntityTypeId() !== 'town_square_content') {
      return FALSE;
    }
    /** @var \Drupal\lgt_town_square\Entity\TownSquareContent $entity */
    if (!$group = $entity->getGroup()) {
      // An associated group is required for all group based CRUD operations, if
      // the group no longer exists, the system cannot validate access or find
      // applicable notification preferences, so the process should end.
      return FALSE;
    }
    return $group->getGroupType()->id() === $this->getGroupType() && in_array($entity->bundle(), array_keys($this->getTownSquareContentTypes()), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function buildApplicableNotificationPreferencesEntityQuery(QueryInterface $query, EntityInterface $entity, string $op): void {
    /** @var \Drupal\lgt_town_square\Entity\TownSquareContent $entity */
    $query->condition('town_square_types', $entity->bundle());

    $group = $entity->getGroup();
    if (!$group) {
      // If the groups can't be loaded from group_content association entity or
      // the IDs stored on the entity during serialization, it means the group
      // the entity once belonged was deleted. If the group no longer exists,
      // there'll be no avenue for access checking and no notifications will be
      // sent for this operation. While rare, in this scenario, the query should
      // return no results.
      throw new UnresolvablePreferenceMatchException('No groups could be resolved for the given entity.');
    }

    $query->condition('town_square_group', $group->id());
  }

  /**
   * {@inheritdoc}
   */
  public function shouldNotify(EntityInterface $content, string $op, NotificationPreference $notificationPreference): bool {
    // Both options this plugin provides are validated in the query component.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationMetadata(EntityInterface $notifyEntity, string $operation): array {
    $metadata = [];
    if ($group = $notifyEntity->getGroup()) {
      $metadata['group'] = $group->id();
    }
    return $metadata;
  }

  /**
   * {@inheritdoc}
   */
  public function summarizeNotificationSettings(NotificationPreference $entity): string {
    if ($entity->town_square_types->count() === count($this->getTownSquareContentTypes())) {
      $type_label = $this->t('All content types');
    }
    else {
      $type_labels = [];
      foreach ($entity->town_square_types as $type) {
        $type_labels[] = $this->getTownSquareContentTypes()[$type->value];
      }
      $type_label = implode($type_labels, ', ');
    }
    return sprintf('%s (%s)', $entity->town_square_group->entity->label(), $type_label);
  }

}

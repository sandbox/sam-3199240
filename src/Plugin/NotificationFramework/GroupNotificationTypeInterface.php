<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

/**
 * An interface for group based notifications.
 */
interface GroupNotificationTypeInterface extends NotificationFrameworkTypeInterface {

  /**
   * Get the content types that a user should be able to subscribe to.
   *
   * @return array
   *   An array of machine name keys, with human readable labels.
   */
  public function getTownSquareContentTypes(): array;

  /**
   * Get the group type the subscription applies to.
   *
   * @return string
   *   The ID of the group.
   */
  public function getGroupType(): string;

}

<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Core\Plugin\PluginBase;

/**
 * Base class for notification type plugins.
 */
abstract class NotificationFrameworkTypeBase extends PluginBase implements NotificationFrameworkTypeInterface {

}

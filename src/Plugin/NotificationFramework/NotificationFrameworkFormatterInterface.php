<?php

namespace Drupal\notification_framework\Plugin\NotificationFramework;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for the formatters of notification emails.
 */
interface NotificationFrameworkFormatterInterface extends PluginInspectionInterface {

  /**
   * Get the subject of a notification email.
   *
   * @param \Drupal\notification_framework\Entity\Notification[] $notifications
   *   An array of notifications being sent.
   *
   * @return string
   *   The subject of an email to send.
   */
  public function getSubject(array $notifications): string;

  /**
   * Get the body of a notifications email.
   *
   * @param \Drupal\notification_framework\Entity\Notification[] $notifications
   *   An array of notifications being sent.
   *
   * @return array
   *   An array of markup.
   */
  public function getBody(array $notifications): array;

  /**
   * Get email attachments for the notification email.
   *
   * @param \Drupal\notification_framework\Entity\Notification[] $notifications
   *   An array of notifications being sent.
   *
   * @return array
   *   An array of attachments.
   */
  public function getAttachments(array $notifications): array;

  /**
   * Check if the plugin is capable of formatting the given notifications.
   *
   * @param \Drupal\notification_framework\Entity\Notification[] $notifications
   *   An array of notifications to format.
   *
   * @return bool
   *   TRUE if the given plugin is applicable, FALSE otherwise.
   */
  public function isApplicable(array $notifications): bool;

}

<?php

namespace Drupal\notification_framework\Plugin\QueueWorker;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\notification_framework\Entity\Notification;
use Drupal\notification_framework\Entity\NotificationSummary;
use Drupal\notification_framework\EntityCrudQueueItem;
use Drupal\notification_framework\NotificationPreferenceMatcher;
use \Drupal\notification_framework\ChunkedIterator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Process CRUD operations for the notifications system.
 *
 * @QueueWorker(
 *   id = "nf_notification_crud",
 *   title = @Translation("Notification CRUD queue worker"),
 *   cron = {"time" = 60}
 * )
 */
class NotificationCrudQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The notification preference matcher.
   *
   * @var \Drupal\notification_framework\NotificationPreferenceMatcher
   */
  protected $matcher;

  /**
   * Preference entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $preferenceStorage;

  /**
   * The memory cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $memoryCache;

  /**
   * NotificationCrudQueueWorker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, NotificationPreferenceMatcher $matcher, ContentEntityStorageInterface $preferenceStorage, MemoryCacheInterface $entityMemoryCache) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->matcher = $matcher;
    $this->preferenceStorage = $preferenceStorage;
    $this->memoryCache = $entityMemoryCache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('notification_framework.preference_matcher'),
      $container->get('entity_type.manager')->getStorage('nf_notification_preference'),
      $container->get('entity.memory_cache')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!$data instanceof EntityCrudQueueItem) {
      throw new \Exception('Invalid data in queue entity crud queue.');
    }

    $preferenceIds = $this->matcher->getValidNotificationPreferences($data->getEntity(), $data->getOperation());
    $notificationIterator = new ChunkedIterator($this->preferenceStorage, $this->memoryCache, $preferenceIds);

    foreach ($notificationIterator->getIterator() as $notificationPreference) {
      // Create one summary for all notifications that are matched by this
      // entity and operation.
      if (!isset($summary)) {
        $summary = NotificationSummary::createFrom($data->getEntity(), $data->getOperation());
        $summary->save();
      }

      // Create the notification entity and send it immediately. At this point,
      // as the queue is being processed, the users access to view the entity
      // has been validated, so it should be assumed that any Notification
      // entity contains content the user had access to at some point in time.
      // It's not necessary to store a copy of the whole entity anymore, just
      // the interesting parts that might be sent in an email or gathered for a
      // digest. Storing these bits of metadata ensure that the notification is
      // true to the state of the entity at the point in which the change was
      // made (regardless of revisions being enabled or not on the source
      // entity) and provides a record and other UIs for viewing streams of
      // changes the user is interested in. By pre-computing key aspects of the
      // display of the notification, like diffs or particular metadata changes
      // the Notification entity also stands as a record of what was sent for
      // a specific version of the notifications code. Adding future features
      // won't necessarily reflect in old Notification entities.
      $notification = Notification::createFrom($summary, $notificationPreference);
      $notification->save();
      if (!$notification->send()) {
        throw new \Exception('Could not send or queue notification email.');
      }
    }
  }

}

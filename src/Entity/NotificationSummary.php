<?php

namespace Drupal\notification_framework\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the notification entity.
 *
 * @ContentEntityType(
 *   id = "nf_notification_summary",
 *   label = @Translation("Notification summary"),
 *   handlers = {
 *     "access" = "Drupal\notification_framework\EntityHandler\NotificationEntityAccessControlHandler",
 *   },
 *   base_table = "nf_notification_summary",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {},
 * )
 */
class NotificationSummary extends ContentEntityBase {

  /**
   * The entity that triggered a notification (if it still exists).
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $notifyEntity;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created time'))
      ->setDescription(t('The time that the notification was created.'));

    $fields['notify_entity_type_id'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notify entity type ID'))
      ->setDescription(t('The ID of the content entity type this notification is for.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['notify_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Notify entity ID'))
      ->setDescription(t('The ID of the content entity this notification is for.'))
      ->setRequired(TRUE);

    $fields['notify_entity_revision_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Notify entity revision ID'))
      ->setDescription(t('The revision ID of the content entity this notification is for.'))
      ->setRequired(TRUE);

    $fields['notify_entity_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notify entity label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['notify_entity_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notify entity bundle'))
      ->setRequired(TRUE)
      ->setSetting('max_length', EntityTypeInterface::BUNDLE_MAX_LENGTH);

    $fields['change_summary'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Change summary'))
      ->setDescription(t('A summary of what has changed or other useful information to include in an email about this notification.'));

    $fields['attachments'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Attachments'))
      ->setDescription(t('Attachments generated by notification summary plugins, to include in the emails sent to users.'));

    $fields['operation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notify entity label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['type_data'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Type data'))
      ->setDescription(t('Data each notification type may want to store, to help format notifications.'));

    return $fields;
  }

  /**
   * Get the operation.
   */
  public function getOperation(): string {
    return $this->operation->value;
  }

  /**
   * Get an array of attachments belonging to this summary.
   */
  public function getAttachments(): array {
    return $this->attachments->getValue();
  }

  /**
   * Get a label representing the operation.
   */
  public function getOperationLabel(): string {
    return ucfirst($this->getOperation());
  }

  /**
   * Get the notifying entity label.
   */
  public function getNotifyLabel(): string {
    return $this->notify_entity_label->value;
  }

  /**
   * Get the notifying entity bundle.
   */
  public function getNotifyBundle(): string {
    return $this->notify_entity_bundle->value;
  }

  /**
   * Get a change summary.
   */
  public function getChangeSummary(): ?string {
    return !$this->change_summary->isEmpty() ? $this->change_summary->value : NULL;
  }

  /**
   * Get the notify entity (if it still exists).
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The notify entity or NULL if it was deleted.
   */
  public function getNotifyEntity(): ?EntityInterface {
    if (isset($this->notifyEntity)) {
      return $this->notifyEntity;
    }
    $storage = \Drupal::entityTypeManager()->getStorage($this->notify_entity_type_id->value);
    // Prefer the exact revision tracked by the notification.
    if ($this->notify_entity_revision_id->value && $revision = $storage->loadRevision($this->notify_entity_revision_id->value)) {
      $this->notifyEntity = $revision;
      return $this->notifyEntity;
    }
    // Fallback to the default revision of the entity.
    if ($entity = $storage->load($this->notify_entity_id->value)) {
      $this->notifyEntity = $entity;
      return $this->notifyEntity;
    }
    // Fallback to NULL, since the entity for old notifications may no longer
    // exist or exist at all in the case of deleted notifications. In general
    // access to the original entity or preference should be optional for the
    // formatting and sending of notifications.
    return NULL;
  }

  /**
   * Create a notification from an entity, op and preference.
   */
  public static function createFrom(EntityInterface $entity, string $operation): NotificationSummary {
    $values = [
      'notify_entity_type_id' => $entity->getEntityTypeId(),
      'notify_entity_id' => $entity->id(),
      'notify_entity_revision_id' => $entity instanceof RevisionableInterface ? $entity->getRevisionId() : NULL,
      'notify_entity_label' => $entity->label(),
      'notify_entity_bundle' => $entity->bundle(),
      'operation' => $operation,
    ];
    /** @var \Drupal\notification_framework\PluginManager\NotificationTypeManager $notification_manager */
    $notification_manager = \Drupal::service('plugin.manager.notification_framework_type');
    $type_plugin = $notification_manager->createInstanceForEntity($entity);
    $values['type_data'] = $type_plugin->getNotificationMetadata($entity, $operation);

    /** @var \Drupal\notification_framework\PluginManager\NotificationSummaryManager $summary_manager */
    $summary_manager = \Drupal::service('plugin.manager.notification_framework_summary');
    /** @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummaryInterface $summary_plugin */
    if ($summary_plugin = $summary_manager->createInstanceForEntity($entity, $operation)) {
      $values['change_summary'] = $summary_plugin->getSummary($entity, $operation);

      $attachments = $summary_plugin->getAttachments($entity, $operation);
      if (!empty($attachments)) {
        $values['attachments'] = $attachments;
      }
    }

    return static::create($values);
  }

}

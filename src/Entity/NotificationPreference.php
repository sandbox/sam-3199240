<?php

namespace Drupal\notification_framework\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\notification_framework\Constant\NotificationParticipation;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the notification preference entity.
 *
 * @ContentEntityType(
 *   id = "nf_notification_preference",
 *   label = @Translation("Notification preference"),
 *   label_collection = @Translation("Notification preferences"),
 *   label_singular = @Translation("Notification preference"),
 *   label_plural = @Translation("notification preferences"),
 *   label_count = @PluralTranslation(
 *     singular = "@count notification preference",
 *     plural = "@count notification preferences",
 *   ),
 *   bundle_label = @Translation("Notification preference type"),
 *   bundle_plugin_type = "notification_framework_type",
 *   handlers = {
 *     "access" = "Drupal\notification_framework\EntityHandler\NotificationPreferenceEntityAccessControlHandler",
 *     "inline_form" = "Drupal\notification_framework\EntityHandler\NotificationPreferenceInlineFormHandler",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *   },
 *   base_table = "nf_notification_preference",
 *   admin_permission = "administer notification preferences",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "owner" = "owner",
 *   },
 * )
 */
class NotificationPreference extends ContentEntityBase implements EntityOwnerInterface {

  use EntityOwnerTrait;

  /**
   * The notification type plugin.
   *
   * @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface
   */
  protected $typePlugin;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type) + static::ownerBaseFieldDefinitions($entity_type);

    $fields['operations'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Notify me when'))
      ->setRequired(TRUE)
      ->setDefaultValue([
        NotificationOperations::CREATED,
        NotificationOperations::COMMENTED,
      ])
      ->setSetting('allowed_values', [
        NotificationOperations::CREATED => t('Content is created'),
        NotificationOperations::COMMENTED => t('Comments are added to content'),
        NotificationOperations::UPDATED => t('Content is updated'),
        NotificationOperations::DELETED => t('Content is deleted'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 11,
      ])
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE);

    // @todo, not yet implemented.
    $fields['participation'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Notify for content'))
      ->setRequired(TRUE)
      ->setDefaultValue(NotificationParticipation::ALL)
      ->setSetting('allowed_values', [
        NotificationParticipation::ALL => t('All content'),
        NotificationParticipation::CREATED => t('Content I created'),
        NotificationParticipation::COMMENTED => t('Content I have commented on'),
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 12,
        // Hide, not yet implemented @todo.
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    /** @var \Drupal\notification_framework\ProfileUserResolver $profile_resolver */
    $profile_resolver = \Drupal::service('notification_framework.profile_user_resolver');
    if (!$account = $profile_resolver->resolveNotificationUser()) {
      throw new \Exception('The owner ID must be provided when creating a notification preference or the preference must be authored on the user notification tab.');
    }
    return $account->id();
  }

  /**
   * Get the type plugin associated with this notification preference.
   *
   * @return \Drupal\notification_framework\Plugin\NotificationFrameworkTypeInterface
   *   The notification type plugin.
   */
  public function getTypePlugin() {
    if (!isset($this->typePlugin)) {
      $manager = \Drupal::service('plugin.manager.notification_framework_type');
      $this->typePlugin = $manager->createInstance($this->bundle());
    }
    return $this->typePlugin;
  }

}

<?php

namespace Drupal\notification_framework\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Mail\MailManager;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the notification entity.
 *
 * @ContentEntityType(
 *   id = "nf_notification",
 *   label = @Translation("Notification"),
 *   handlers = {
 *     "access" = "Drupal\notification_framework\EntityHandler\NotificationEntityAccessControlHandler",
 *   },
 *   base_table = "nf_notification",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "owner",
 *   },
 *   links = {},
 * )
 */
class Notification extends ContentEntityBase implements EntityOwnerInterface {

  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type)
      + static::ownerBaseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created time'))
      ->setDescription(t('The time that the notification was created.'));

    $fields['notification_summary'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notification summary'))
      ->setDescription(t('The notification summary'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'nf_notification_summary');

    $fields['notification_preference'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Notification preference'))
      ->setDescription(t('The notification preference'))
      ->setRequired(FALSE)
      ->setSetting('target_type', 'nf_notification_preference');

    // Store the notification preference type explicitly, since the original
    // notification preference may have been deleted.
    $fields['notification_preference_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Notification preference type'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['email_sent'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Email sent'))
      ->setDescription(t('A boolean indicating if an email was sent.'))
      ->setDefaultValue(FALSE);

    $fields['email_sent_time'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Email sent time'))
      ->setDescription(t('The time the email was sent.'));

    return $fields;
  }

  /**
   * Get the notification summary.
   */
  public function getSummary(): NotificationSummary {
    return $this->notification_summary->entity;
  }

  /**
   * {@inheritdoc}
   */
  public static function getDefaultEntityOwner() {
    return NULL;
  }

  /**
   * Create a notification from an entity, op and preference.
   */
  public static function createFrom(NotificationSummary $summary, NotificationPreference $preference): Notification {
    $values = [
      'notification_preference' => $preference->id(),
      'notification_preference_type' => $preference->bundle(),
      'notification_summary' => $summary->id(),
      'owner' => $preference->getOwnerId(),
    ];
    return static::create($values);
  }

  /**
   * Get the notification type used to trigger this preference.
   */
  public function getNotificationType(): NotificationFrameworkTypeInterface {
    return \Drupal::service('plugin.manager.notification_framework_type')->createInstance($this->notification_preference_type->value);
  }

  /**
   * Send an email for this notification.
   *
   * @return bool
   *   TRUE on success, FALSE on failure.
   */
  public function send(): bool {
    if ($this->isNew()) {
      throw new \Exception('Save a notification preference before sending it.');
    }
    $sent_email = $this->getMailManager()->mail('notification_framework', 'notification', $this->getOwner()->getEmail(), 'en', [
      'notification' => $this->id(),
    ]);
    // Ensure the email is either sent or queued.
    if ($sent_email['result'] || $sent_email['queued']) {
      $this->email_sent->value = TRUE;
      $this->email_sent_time->value = \Drupal::time()->getCurrentTime();
      $this->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Get the mail manager.
   */
  protected function getMailManager(): MailManager {
    return \Drupal::service('plugin.manager.mail');
  }

}

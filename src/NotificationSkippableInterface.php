<?php

namespace Drupal\notification_framework;

/**
 * An interface for skipping notifications.
 *
 * @see \Drupal\notification_framework\NotificationSkippableTrait
 */
interface NotificationSkippableInterface {

  /**
   * Indicate notifications should be skipped when saving an entity.
   */
  public function skipNotifications(): void;

  /**
   * Check if notifications have been skipped for a specific save.
   *
   * @return bool
   *   TRUE if notifications have been skipped, FALSE otherwise.
   */
  public function isNotificationsSkipped(): bool;

}

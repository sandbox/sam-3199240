<?php

namespace Drupal\notification_framework\EntityHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * The NotificationPreferenceEntityAccessControlHandler class.
 */
class NotificationPreferenceEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($entity_bundle === NULL) {
      throw new \Exception('Notification preference must have a bundle.');
    }

    /** @var \Drupal\notification_framework\ProfileUserResolver $profile_user_resolver */
    $profile_user_resolver = \Drupal::service('notification_framework.profile_user_resolver');
    // If a user has access to edit users, they can implicitly administer
    // notifications for that user as well. So that admins can't accidentally
    // create notifications for other users they may not be able to create
    // themselves, access check notification preferences by the profile being
    // viewed, not necessarily the logged in user.
    if ($profile_user = $profile_user_resolver->resolveNotificationUser()) {
      $account = $profile_user;
    }

    if ($account->hasPermission('nf administer notification preferences')) {
      return AccessResult::allowed()->cachePerPermissions();
    }
    /** @var \Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeInterface $type_plugin */
    $type_plugin = \Drupal::service('plugin.manager.notification_framework_type')->createInstance($entity_bundle);
    // Delegate to each type plugin to see if the user has access to create
    // notifications of the given bundle.
    return AccessResult::allowedIfHasPermission($account, 'nf subscribe to notifications')
      ->andIf($type_plugin->checkCreateAccess($account));
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\notification_framework\Entity\NotificationPreference $entity */
    if ($account->hasPermission('nf administer notification preferences')) {
      return AccessResult::allowed()->cachePerPermissions();
    }
    return AccessResult::allowedIf(!$account->isAnonymous() && $entity->getOwnerId() === $account->id())->cachePerUser();
  }

}

<?php

namespace Drupal\notification_framework\EntityHandler;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\inline_entity_form\Form\EntityInlineForm;
use Drupal\notification_framework\Entity\NotificationPreference;

/**
 * An inline form handler for the notification preference entity type.
 */
class NotificationPreferenceInlineFormHandler extends EntityInlineForm {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function isTableDragEnabled($element) {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = [];
    $fields['label'] = [
      'label' => $this->t('Notification type'),
      'type' => 'callback',
      'callback' => [static::class, 'renderLabel'],
      'weight' => 1,
    ];
    $fields['notification'] = [
      'label' => $this->t('Notification'),
      'type' => 'callback',
      'callback' => [static::class, 'renderNotification'],
      'weight' => 2,
    ];
    $fields['operations'] = [
      'label' => $this->t('Notify when'),
      'type' => 'field',
      'weight' => 2,
    ];
    // @codingStandardsIgnoreStart
//    $fields['participation'] = [
//      'label' => $this->t('Notify for'),
//      'type' => 'field',
//      'weight' => 2,
//    ];
    // @codingStandardsIgnoreEnd

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function renderLabel(NotificationPreference $entity, $context) {
    return $entity->getTypePlugin()->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public static function renderNotification(NotificationPreference $entity, $context) {
    return $entity->getTypePlugin()->summarizeNotificationSettings($entity);
  }

}

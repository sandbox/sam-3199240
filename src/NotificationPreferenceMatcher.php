<?php

namespace Drupal\notification_framework;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\notification_framework\Exception\UnresolvablePreferenceMatchException;
use Drupal\notification_framework\PluginManager\NotificationTypeManager;
use \Drupal\notification_framework\ChunkedIterator;

/**
 * Match an entity and operation to notification preferences.
 */
class NotificationPreferenceMatcher {

  /**
   * The notification type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationTypeManager
   */
  protected $notificationTypeManager;

  /**
   * Storage for the notification preference entity type.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected $notificationPreferenceStorage;

  /**
   * The entity memory cache.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $entityMemoryCache;

  /**
   * EntityCrudHooks constructor.
   */
  public function __construct(NotificationTypeManager $notificationTypeManager, EntityTypeManagerInterface $entityTypeManager, MemoryCacheInterface $entityMemoryCache) {
    $this->notificationTypeManager = $notificationTypeManager;
    $this->notificationPreferenceStorage = $entityTypeManager->getStorage('nf_notification_preference');
    $this->entityMemoryCache = $entityMemoryCache;
  }

  /**
   * Get valid notification preferences that match the entity and operation.
   */
  public function getValidNotificationPreferences(EntityInterface $entity, string $operation): array {
    try {
      return $this->filterApplicableNotificationPreferences(
        $this->queryApplicableNotificationPreferences($entity, $operation),
        $entity,
        $operation
      );
    }
    catch (UnresolvablePreferenceMatchException $e) {
      // Catch exceptions designed to indicate exceptional circumstances where
      // an entity is not in a state to send any notifications.
      return [];
    }
  }

  /**
   * Get the notification preference applicable to the entity and operation.
   *
   * This query is not an exhaustive way of finding the exact preferences that
   * match the given entity type and operation. It's goal is only to query for
   * the smallest subset of notification preferences possible. There are still
   * aspects of each entity, preference and user that can't matched by a query,
   * which will need to be resolved after the query is executed, however it
   * allows less work to be done for each event that potentially triggers a
   * notification.
   *
   * @return array
   *   An array of preference IDs matching the entity and operation.
   */
  protected function queryApplicableNotificationPreferences(EntityInterface $entity, string $operation): array {
    // Load the type plugin that handles notifications for the given entity.
    if (!$type_plugin = $this->notificationTypeManager->createInstanceForEntity($entity)) {
      return [];
    }

    // Find notification preferences with the bundle of the type plugin
    // responsible for this entity and for the given operation.
    $query = $this->notificationPreferenceStorage->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('type', $type_plugin->getPluginId());
    $query->condition('operations', $operation);

    // Delegate to the type plugin to add any additional conditions to the
    // query to filter the list of applicable notification preference entities.
    $type_plugin->buildApplicableNotificationPreferencesEntityQuery($query, $entity, $operation);

    return $query->execute();
  }

  /**
   * Filter individual notification preferences down to sendable preferences.
   */
  protected function filterApplicableNotificationPreferences(array $candidateIds, EntityInterface $entity, string $notifyOperation): array {
    $notificationIterator = new ChunkedIterator($this->notificationPreferenceStorage, $this->entityMemoryCache, $candidateIds);
    $type_plugin = $this->notificationTypeManager->createInstanceForEntity($entity);

    $valid_preferences = [];

    /** @var \Drupal\notification_framework\Entity\NotificationPreference $notification_preference */
    foreach ($notificationIterator->getIterator() as $notification_preference) {
      // Filter by entities the owner of the notification has access to view.
      if (!$entity->access('view', $notification_preference->getOwner())) {
        continue;
      }
      // Do not allow users who are blocked to receive notifications.
      if ($notification_preference->getOwner()->isBlocked()) {
        continue;
      }
      // Allow each type plugin to add additional qualifications to see if the
      // given notification preferences matches.
      if (!$type_plugin->shouldNotify($entity, $notifyOperation, $notification_preference)) {
        continue;
      }
      $valid_preferences[] = $notification_preference->id();
    }

    return $valid_preferences;
  }

}

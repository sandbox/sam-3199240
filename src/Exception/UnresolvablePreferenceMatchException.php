<?php

namespace Drupal\notification_framework\Exception;

/**
 * An exception thrown during preference matching.
 *
 * Throwing this exception during the preference matching process means the
 * appropriate context could not be derived to match an entity to a set of
 * preferences and the system should not attempt to continue the notification
 * process.
 */
class UnresolvablePreferenceMatchException extends \Exception {
}

<?php

namespace Drupal\notification_framework;

use Drupal\Core\Entity\EntityInterface;

/**
 * The entity crud queue item class.
 */
class EntityCrudQueueItem {

  /**
   * The entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * The operation.
   *
   * @var string
   */
  protected $operation;

  /**
   * Create an instance of the queue item from an entity and operation.
   */
  public static function create(EntityInterface $entity, string $operation): EntityCrudQueueItem {
    $instance = new static();
    $instance->entity = $entity;
    $instance->operation = $operation;
    return $instance;
  }

  /**
   * Get the entity that was queued.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Get the operation that was queued.
   */
  public function getOperation(): string {
    return $this->operation;
  }

}

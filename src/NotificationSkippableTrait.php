<?php

namespace Drupal\notification_framework;

/**
 * A trait to implement NotificationSkippableInterface.
 *
 * @see \Drupal\notification_framework\NotificationSkippableInterface
 */
trait NotificationSkippableTrait {

  /**
   * Flag to indicate if notifications are skipped.
   *
   * @var bool
   */
  protected $skipNotifications = FALSE;

  /**
   * {@inheritdoc}
   */
  public function skipNotifications(): void {
    $this->skipNotifications = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isNotificationsSkipped(): bool {
    return $this->skipNotifications;
  }

}

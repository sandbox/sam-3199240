<?php

namespace Drupal\notification_framework\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Annotation for notification formatters.
 *
 * @Annotation
 */
class NotificationFrameworkFormatter extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * Label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}

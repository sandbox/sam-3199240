<?php

namespace Drupal\notification_framework;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\user\UserInterface;

/**
 * The ProfileUserResolver class.
 */
class ProfileUserResolver {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * ProfileUserResolver constructor.
   */
  public function __construct(RouteMatchInterface $routeMatch) {
    $this->routeMatch = $routeMatch;
  }

  /**
   * Resolve the notification user.
   *
   * Loads the user profile that is being edited from the notifications screen.
   * This is useful because in a lot of cases such as the default entity owner
   * of notifications and access checking to bundles and other resources, admins
   * should not be allowed to configure a set of notifications for another user
   * that does not have the same level of access as they do.
   */
  public function resolveNotificationUser() : ?UserInterface {
    if ($this->routeMatch->getRouteName() !== 'notification_framework_ui.notifications') {
      return NULL;
    }
    return $this->routeMatch->getParameter('user');
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;


/**
 * Test the notifications table.
 */
class NotificationsUiTest extends NotificationFrameworkFunctionalTestBase {

  /**
   * Test the display of the notifications table.
   *
   * Test that the correct bundle the user has access to is created when using
   * IEF, @see composer patches.
   */
  public function testAddNotificationSingleBundle() {
    // Create a user who is a member of a group, but has no access to the
    // officer view.
    $group = $this->createGroup('town_square');
    $account = $this->createGroupMember($group);

    $this->drupalLogin($account);
    $this->visitNotifications($account);
    $this->submitForm([], 'Add Town Square notification');
    $this->submitForm([
      'field_notifications[form][0][town_square_group]' => $group->id(),
    ], 'Create Notification');
    $this->submitForm([], 'Save');

    /** @var \Drupal\notification_framework\Entity\NotificationPreference $notification */
    $notification = $this->lastCreatedEntity('nf_notification_preference');
    $this->assertEquals($account->id(), $notification->getOwnerId());
    $this->assertEquals('town_square', $notification->bundle());
  }

  /**
   * Test the warning appears when updating.
   */
  public function testNotificationUpdateWarning() {
    $group = $this->createGroup('town_square');
    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
    ]);

    $this->drupalLogin($account);
    $this->visitNotifications($account);

    $this->assertSession()->pageTextNotContains('You have unsaved changes.');
    $this->submitForm([], 'Edit');
    $this->submitForm([], 'Update');
    $this->assertSession()->pageTextContains('You have unsaved changes.');
  }

  /**
   * Test the warning appears when adding.
   */
  public function testNotificationAddWarning() {
    $group = $this->createGroup('town_square');
    $account = $this->createGroupMember($group);
    $this->drupalLogin($account);
    $this->visitNotifications($account);

    $this->assertSession()->pageTextNotContains('You have unsaved changes.');
    $this->submitForm([], 'Add Town Square notification');
    $this->submitForm([
      'field_notifications[form][0][town_square_group]' => $group->id(),
    ], 'Create Notification');
    $this->assertSession()->pageTextContains('You have unsaved changes.');
    $this->submitForm([], 'Save');
  }

  /**
   * Test the warning appears when deleting.
   */
  public function testNotificationsDeleteWarning() {
    $group = $this->createGroup('town_square');
    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
    ]);

    $this->drupalLogin($account);
    $this->visitNotifications($account);

    $this->assertSession()->pageTextNotContains('You have unsaved changes.');
    $this->submitForm([], 'Remove');
    $this->submitForm([], 'Remove');
    $this->assertSession()->pageTextContains('You have unsaved changes.');
  }

  /**
   * Test admin user notification access.
   *
   * Test admins have access to create notifications based on the profile they
   * are viewing, not the notifications they have access to.
   */
  public function testAdminUserNotificationAccess() {
    $admin = $this->createToolboxAdmin();
    $this->drupalLogin($admin);

    $this->visitNotifications($admin);
    $this->assertSession()->buttonExists('Add Officer View notification');
    $this->assertSession()->buttonExists('Add Town Square notification');

    $this->visitNotifications($this->createOfficer());
    $this->assertSession()->buttonExists('Add Officer View notification');
    $this->assertSession()->buttonNotExists('Add Town Square notification');

    $this->visitNotifications($this->createGroupMember($this->createGroup('town_square')));
    $this->assertSession()->buttonNotExists('Add Officer View notification');
    $this->assertSession()->buttonExists('Add Town Square notification');
  }

}

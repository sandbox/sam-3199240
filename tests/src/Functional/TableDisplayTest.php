<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\notification_framework\Constant\NotificationParticipation;

/**
 * Test the notifications table.
 */
class TableDisplayTest extends NotificationFrameworkFunctionalTestBase {

  /**
   * Test the display of the notifications table.
   */
  public function testNotificationTableDisplay() {
    $group = $this->createGroup('town_square', 'Foo group');
    $account = $this->createGroupMember($group);

    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::COMMENTED,
        NotificationOperations::DELETED,
      ],
      'participation' => NotificationParticipation::CREATED,
    ]);

    $this->drupalLogin($account);
    $this->visitNotifications($account);

    $this->assertTableHeaderContains(1, 'Notification type');
    $this->assertTableHeaderContains(2, 'Notification');
    $this->assertTableHeaderContains(3, 'Notify when');
    $this->assertTableHeaderContains(4, 'Operations');

    $this->assertTableCellContains(1, 1, 'Town square');
    $this->assertTableCellContains(1, 2, 'Foo group (All content types)');
    $this->assertTableCellContains(1, 3, 'Comments are added to content');
    $this->assertTableCellContains(1, 3, 'Content is deleted');
    $this->assertTableCellContains(1, 4, 'Edit');
    $this->assertTableCellContains(1, 4, 'Remove');
  }

}

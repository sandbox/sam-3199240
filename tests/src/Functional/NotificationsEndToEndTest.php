<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * Test the notifications end to end.
 */
class NotificationsEndToEndTest extends NotificationFrameworkFunctionalTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearMail();
    $this->clearQueue('nf_notification_crud');
    $this->container->get('state')->set('notification_framework_enforce_skip', FALSE);
  }

  /**
   * Test the notifications end to end.
   */
  public function testNotificationsEndToEnd() {
    $group = $this->createTownSquare('Foogroup');
    $account = $this->createGroupMember($group);

    $this->drupalLogin($account);

    // The user should already be subscribed to most notifications, with the
    // exception of the updated and deleted op.
    $this->visitNotifications($account);
    $this->assertSession()->elementContains('css', '.user-notifications-form', 'Foogroup (All content types)');

    // Edit the notification and add all operations.
    $this->submitForm([], 'Edit');
    $this->submitForm([
      'field_notifications[form][inline_entity_form][entities][0][form][operations][created]' => TRUE,
      'field_notifications[form][inline_entity_form][entities][0][form][operations][commented]' => TRUE,
      'field_notifications[form][inline_entity_form][entities][0][form][operations][updated]' => TRUE,
      'field_notifications[form][inline_entity_form][entities][0][form][operations][deleted]' => TRUE,
    ], 'Update Notification');
    $this->submitForm([], 'Save');

    // The admin too will get all notifications.
    $group_admin = $this->createGroupAdmin($group);
    $this->drupalLogin($group_admin);
    $this->visitNotifications($group_admin);
    $this->assertSession()->elementContains('css', '.user-notifications-form', 'Officer view');
    $this->assertSession()->elementContains('css', '.user-notifications-form', 'Foogroup (All content types)');

    $this->drupalPostForm("group/{$group->id()}/content/create/group_town_square:discussion", [
      'title[0][value]' => 'Sample discussion',
    ], 'Save');
    $this->assertSession()->pageTextContains('Your discussion has been saved');

    $this->clickLink('Edit');
    $this->submitForm([
      'title[0][value]' => 'Updated sample discussion',
    ], 'Save');
    $this->assertSession()->pageTextContains('Your discussion has been saved');

    $this->clickLink('Delete');
    $this->submitForm([], 'Delete');

    $this->processQueue('nf_notification_crud');

    $mail = $this->assertMail()
      ->seekToModule('notification_framework')
      ->seekToRecipient($account->getEmail())
      ->countEquals(3);
    $mail->seekToIndex(0)->subjectEquals('Foogroup Discussion "Sample discussion" created');
    $mail->seekToIndex(1)->subjectEquals('Foogroup Discussion "Updated sample discussion" updated');
    $mail->seekToIndex(2)->subjectEquals('Foogroup Discussion "Updated sample discussion" deleted');
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\Core\Entity\EntityInterface;
use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\notification_framework\Constant\NotificationOperations;

/**
 * @coversDefaultClass \Drupal\notification_framework\NotificationPreferenceMatcher
 */
class NotificationPreferenceMatcherTest extends NotificationFrameworkKernelTestBase {

  /**
   * The preference matcher.
   *
   * @var \Drupal\notification_framework\NotificationPreferenceMatcher
   */
  protected $matcher;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->matcher = $this->container->get('notification_framework.preference_matcher');
  }

  /**
   * Test matching an entity and operation to a given preference.
   */
  public function testMatchTownSquareNotificationType() {
    $group = $this->createGroup('town_square');
    $unrelated_group = $this->createGroup('town_square');
    $user = $this->createGroupMember($group);

    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'key_decisions',
        'generic_register',
        'page',
      ],
    ]);

    // Notifications will be resolved for matching group, content and operation.
    $this->assertMatcherResolves($preference, $this->createGenericRegister($group), NotificationOperations::UPDATED);
    // Notifications should not resolve from non-subscribed groups.
    $this->assertMatcherNotResolves($preference, $this->createGenericRegister($unrelated_group), NotificationOperations::UPDATED);
    // Notifications will not be resolved for non-matching operations.
    $this->assertMatcherNotResolves($preference, $this->createGenericRegister($group), NotificationOperations::CREATED);
    // Notifications will not be resolved for non-matching content types.
    $this->assertMatcherNotResolves($preference, $this->createPlannedOutage($group), NotificationOperations::CREATED);
  }

  /**
   * Test matching an entity and operation to a given preference.
   */
  public function testMatchOfficerViewNotificationType() {
    $user = $this->createOfficer();
    $preference = $this->createNotificationPreference($user, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::DELETED,
      ],
      'officer_view_types' => [
        'discussion',
      ],
    ]);

    $this->assertMatcherResolves($preference, $this->createDiscussion($this->createUser()), NotificationOperations::DELETED);
    $this->assertMatcherNotResolves($preference, $this->createDiscussion($this->createUser()), NotificationOperations::UPDATED);
    $this->assertMatcherNotResolves($preference, $this->createCouncilPage(), NotificationOperations::UPDATED);
  }

  /**
   * Test filtering access to notification preferences.
   */
  public function testAccessFilteredNotificationPreferences() {
    $group = $this->createGroup('town_square');
    $unrelated_group = $this->createGroup('town_square');
    $user = $this->createGroupMember($group);

    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);
    // Create a preference for the user that they don't actually have access to
    // view the content for.
    $unrelated_preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $unrelated_group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);

    $this->assertMatcherResolves($preference, $this->createKeyDecision($group), NotificationOperations::UPDATED);
    $this->assertMatcherNotResolves($unrelated_preference, $this->createKeyDecision($unrelated_group), NotificationOperations::UPDATED);
  }

  /**
   * Test filtering with a blocked user.
   */
  public function testBlockedUserFilteredNotificationPreferences() {
    $group = $this->createGroup('town_square');
    $user = $this->createGroupMember($group);
    $user->block();
    $user->save();

    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);
    $this->assertMatcherNotResolves($preference, $this->createKeyDecision($group), NotificationOperations::UPDATED);
  }

  /**
   * Test the matcher with deleted group content.
   */
  public function testMatcherDeletedGroup() {
    $group = $this->createGroup('town_square');
    $user = $this->createGroupMember($group);

    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);
    $key_decision = $this->createKeyDecision($group);

    $this->assertMatcherResolves($preference, $key_decision, NotificationOperations::UPDATED);
    $group->delete();
    $this->assertMatcherNotResolves($preference, $key_decision, NotificationOperations::UPDATED);
  }

  /**
   * Test access filtering with an admin account.
   */
  public function testAccessFilteredAdminAccess() {
    $user = $this->createToolboxAdmin();
    $group = $this->createGroup('town_square');
    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);
    $this->assertMatcherResolves($preference, $this->createKeyDecision($group), NotificationOperations::UPDATED);
  }

  /**
   * Assert the matcher resolves a preference from the given entity and op.
   */
  protected function assertMatcherResolves(NotificationPreference $expected_preference, EntityInterface $entity, string $operation) {
    $matched_preferences = $this->matcher->getValidNotificationPreferences($entity, $operation);
    $this->assertContains($expected_preference->id(), $matched_preferences);
  }

  /**
   * Assert the matcher resolves a preference from the given entity and op.
   */
  protected function assertMatcherNotResolves(NotificationPreference $expected_preference, EntityInterface $entity, string $operation) {
    $matched_preferences = $this->matcher->getValidNotificationPreferences($entity, $operation);
    $this->assertNotContains($expected_preference->id(), $matched_preferences);
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * @coversDefaultClass \Drupal\notification_framework\Hooks\EntityCrudHooks
 */
class EntityCrudHooksTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearQueue('nf_notification_crud');
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testCrudHooks() {
    $this->assertQueueSize(0, 'nf_notification_crud');

    $group = $this->createGroup('town_square');
    $entity = $this->createGroupDiscussion($group);
    $this->assertQueueSize(1, 'nf_notification_crud');

    $entity->title = 'updated';
    $entity->save();
    $this->assertQueueSize(2, 'nf_notification_crud');

    $this->createTownSquareDiscussionComment($this->createUser(), $entity);
    $this->assertQueueSize(3, 'nf_notification_crud');

    $entity->delete();
    $this->assertQueueSize(4, 'nf_notification_crud');
  }

  /**
   * Test skip queuing for syncing entities.
   */
  public function testSkipQueueForSyncing() {
    $this->assertQueueSize(0, 'nf_notification_crud');

    $group = $this->createGroup('town_square');
    $entity = $this->createGroupDiscussion($group);
    $this->assertQueueSize(1, 'nf_notification_crud');

    $entity->title = 'updated';
    $entity->setSyncing(TRUE);
    $entity->save();
    $this->assertQueueSize(1, 'nf_notification_crud');
  }

  /**
   * Test skip queuing for syncing entities.
   */
  public function testSkipQueueForCustomInterface() {
    $this->assertQueueSize(0, 'nf_notification_crud');

    $document = $this->createDocument(NULL, NULL, NULL, [
      'name' => 'Sample document',
    ]);
    $this->assertQueueSize(1, 'nf_notification_crud');

    $document->title = 'updated';
    $document->skipNotifications();
    $document->save();
    $this->assertQueueSize(1, 'nf_notification_crud');
  }

}

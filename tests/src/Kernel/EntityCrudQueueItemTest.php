<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\EntityCrudQueueItem;

/**
 * @coversDefaultClass \Drupal\notification_framework\EntityCrudQueueItem
 */
class EntityCrudQueueItemTest extends NotificationFrameworkKernelTestBase {

  /**
   * Test access checks against unserialized, deleted entities.
   */
  public function testTownSquareCrudQueueItemDeletedEntityAccess() {
    $group = $this->createGroup('town_square');
    $entity = $this->createKeyDecision($group);

    $item = EntityCrudQueueItem::create($entity, 'foo');

    $serialized = serialize($item);
    $entity->delete();
    $unserialized = unserialize($serialized);

    $user = $this->createUser();
    $group_user = $this->createGroupMember($group);

    $this->assertTrue($unserialized->getEntity()->access('view', $group_user));
    $this->assertFalse($unserialized->getEntity()->access('view', $user));
  }

  /**
   * Test access checks against unserialized, deleted entities.
   */
  public function testOfficerCrudQueueItemDeletedEntityAccess() {
    $entity = $this->createDiscussion();
    $item = EntityCrudQueueItem::create($entity, 'foo');

    $serialized = serialize($item);
    $entity->delete();
    $unserialized = unserialize($serialized);

    $user = $this->createUser();
    $officer = $this->createOfficer();

    $this->assertTrue($unserialized->getEntity()->access('view', $officer));
    $this->assertFalse($unserialized->getEntity()->access('view', $user));
  }

}

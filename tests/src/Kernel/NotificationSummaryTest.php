<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\Core\Render\Markup;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * Test the summaries attached to notifications.
 */
class NotificationSummaryTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearMail();
    $this->clearQueue('nf_notification_crud');
  }

  /**
   * Test the format of notification emails.
   */
  public function testDiscussionSummaries() {
    $group = $this->createTownSquare('Foosquare');
    $account = $this->createAdminUser();

    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::COMMENTED,
      ],
      'town_square_types' => ['discussion'],
    ]);
    $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::COMMENTED,
      ],
      'officer_view_types' => ['discussion'],
    ]);

    $tsq_discussion = $this->createGroupDiscussion($group, NULL, [
      'field_body' => [
        'format' => 'full_html',
        'value' => '<p>TSQ discussion body</p>',
      ],
    ]);
    $tsq_comment = $this->createTownSquareDiscussionComment($this->createOfficer(), $tsq_discussion, 'Sample comment', 'Sample TSQ comment body');

    $officer_discussion = $this->createDiscussion(NULL, NULL, [
      'title' => 'Officer discussion',
      'body' => [
        'value' => 'Sample officer discussion',
        'format' => 'basic_html',
      ],
    ]);
    $officer_comment = $this->createDiscussionComment($account, $officer_discussion, 'Sample officer comment', 'Sample officer comment');

    $this->processQueue('nf_notification_crud');

    // Four emails should have been sent to the user.
    $mail = $this->assertMail()->seekToRecipient($account->getEmail());
    $mail->countEquals(4);

    // Each should contain a summary of the discussion.
    $mail->seekToIndex(0)->bodyContains('TSQ discussion body');
    $mail->seekToIndex(1)->bodyContains('Sample TSQ comment body');
    $mail->seekToIndex(2)->bodyContains('Sample officer discussion');
    $mail->seekToIndex(3)->bodyContains('Sample officer comment');

    // All should contain a link to reply to the discussion.
    $mail->bodyContains('Reply to this discussion');
  }

  /**
   * Test discussion comment summaries with deleted authors.
   */
  public function testDiscussionCommentSummaryDeletedAuthor() {
    $account = $this->createAdminUser();
    $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::COMMENTED,
      ],
      'officer_view_types' => ['discussion'],
    ]);

    $discussion = $this->createDiscussion(NULL, NULL, [
      'title' => 'Officer discussion',
      'body' => [
        'value' => 'Sample officer discussion',
        'format' => 'basic_html',
      ],
    ]);

    $comment_author = $this->createOfficer();
    $this->createDiscussionComment($comment_author, $discussion, 'Sample officer comment', 'Sample officer comment');
    $comment_author->delete();

    $this->processQueue('nf_notification_crud');

    // An email should have been sent, despite the author of the comment being
    // deleted.
    $mail = $this->assertMail()->seekToRecipient($account->getEmail());
    $mail->countEquals(1)->bodyContains('Unknown author');
  }

  /**
   * Test generating a summary of meetings.
   */
  public function testMeetingSummary() {
    $group = $this->createTownSquare('Sample square');
    $account = $this->createAdminUser();

    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => ['meeting'],
    ]);

    $meeting = $this->createMeeting($group, [
      'field_meeting_date_range' => [
        'value' => '2016-11-04T00:21:00',
        'end_value' => '2016-11-04T00:26:00',
      ],
      'field_meeting_location' => 'Foo location',
      'field_description' => [
        'value' => '<p>Sample description<p>',
        'format' => 'basic_html',
      ],
      'title' => 'Sample meeting',
      'uuid' => '0b4460f4-4909-4663-9f25-8e82311d605c',
      'changed' => 123456,
    ]);
    $meeting->delete();

    $this->processQueue('nf_notification_crud');

    $this->assertMail()
      ->countEquals(2)
      ->bodyContains('Sample description')
      ->bodyContains('Foo location');

    $this->assertMail()->seekToIndex(0)->attachmentsEqual([
      [
        'filecontent' => Markup::create('BEGIN:VCALENDAR
PRODID:-//NF//Email Notification//EN
VERSION:2.0
BEGIN:VTIMEZONE
TZID:Australia/Brisbane
BEGIN:STANDARD
DTSTART:19920229T160000
TZOFFSETFROM:+1100
TZOFFSETTO:+1000
TZNAME:AEST
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19911026T160000
TZOFFSETFROM:+1000
TZOFFSETTO:+1100
TZNAME:AEDT
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
STATUS:CONFIRMED
UID:0b4460f4490946639f258e82311d605c
SEQUENCE:123456
DTSTART;TZID=Australia/Brisbane:20161104T102100
DTEND;TZID=Australia/Brisbane:20161104T102600
SUMMARY:Sample meeting
LOCATION:Foo location
DESCRIPTION:Sample description
X-ALT-DESC;FMTTYPE=text/html:<div class="clearfix text-formatted field field--name-field-description field--type-text-long field--label-hidden field__item"><p>Sample description</p></div>
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR
'),
        'filename' => 'invite.ics',
        'filemime' => 'text/calendar',
      ],
    ]);

    $this->assertMail()->seekToIndex(1)->attachmentsEqual([
      [
        'filecontent' => Markup::create('BEGIN:VCALENDAR
PRODID:-//NF//Email Notification//EN
VERSION:2.0
BEGIN:VTIMEZONE
TZID:Australia/Brisbane
BEGIN:STANDARD
DTSTART:19920229T160000
TZOFFSETFROM:+1100
TZOFFSETTO:+1000
TZNAME:AEST
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19911026T160000
TZOFFSETFROM:+1000
TZOFFSETTO:+1100
TZNAME:AEDT
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VEVENT
STATUS:CANCELLED
UID:0b4460f4490946639f258e82311d605c
SEQUENCE:123456
DTSTART;TZID=Australia/Brisbane:20161104T102100
DTEND;TZID=Australia/Brisbane:20161104T102600
SUMMARY:Sample meeting
LOCATION:Foo location
DESCRIPTION:Sample description
X-ALT-DESC;FMTTYPE=text/html:<div class="clearfix text-formatted field field--name-field-description field--type-text-long field--label-hidden field__item"><p>Sample description</p></div>
TRANSP:OPAQUE
END:VEVENT
END:VCALENDAR
'),
        'filename' => 'invite.ics',
        'filemime' => 'text/calendar',
      ],
    ]);
  }

  /**
   * Test the summary of documents.
   */
  public function testBareDocumentSummary() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
    ]);
    $document = $this->createDocument(NULL, NULL, NULL, [
      'name' => 'Sample doc',
      'description' => NULL,
    ]);

    $this->processQueue('nf_notification_crud');

    $this->assertMail()
      ->countEquals(1)
      ->bodyContains('*Content operation:* Created')
      ->bodyContains('*Document type:* Unspecified')
      ->bodyContains('*Topic:* Unspecified')
      ->bodyContains('*Description:* Unspecified')
      ->subjectEquals('Document "Sample doc" created');
  }

  /**
   * Test the summary of documents.
   */
  public function testFullDocumentSummary() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
    ]);
    $topic = $this->createOfficerTopic('Sample topic');
    $type = $this->createDocumentType('Sample type');
    $document = $this->createDocument($this->createCouncil('Foo council'), NULL, NULL, [
      'name' => 'Sample doc',
      'topic' => ['entity' => $topic],
      'document_type' => ['entity' => $type],
      'description' => [
        'value' => '<p>Sample</p>',
        'format' => 'full_html',
      ],
    ]);

    $this->processQueue('nf_notification_crud');

    $this->assertMail()
      ->countEquals(1)
      ->bodyContains('*Content operation:* Created')
      ->bodyContains('*Document type:* Sample type')
      ->bodyContains('*Topic:* Sample topic')
      ->bodyContains("*Description:*\n    Sample")
      ->bodyContains('*Council:* Foo council')
      ->subjectEquals('Document "Sample doc" created');
  }

}

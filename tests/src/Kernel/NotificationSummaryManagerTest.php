<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummary\CommentedContent;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummary\GroupDiscussion;

/**
 * @coversDefaultClass \Drupal\notification_framework\PluginManager\NotificationSummaryManager
 */
class NotificationSummaryManagerTest extends NotificationFrameworkKernelTestBase {

  /**
   * The summary manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationSummaryManager
   */
  protected $summaryManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->summaryManager = $this->container->get('plugin.manager.notification_framework_summary');
  }

  /**
   * @covers ::createInstanceForEntity
   */
  public function testCreateInstanceForEntity() {
    $group = $this->createTownSquare('Sample square');
    $discussion = $this->createTownSquareDiscussion($group, NULL, ['field_body' => 'foo']);
    $summary = $this->summaryManager->createInstanceForEntity($discussion, NotificationOperations::CREATED);
    $this->assertInstanceOf(GroupDiscussion::class, $summary);

    $discussion->_notificationComment = $this->createTownSquareDiscussionComment($this->createGroupMember($group), $discussion);
    $summary = $this->summaryManager->createInstanceForEntity($discussion, NotificationOperations::COMMENTED);
    $this->assertInstanceOf(CommentedContent::class, $summary);
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * The NotificationCrudQueueWorkerTest class.
 */
class FailingMailerTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected $mailPlugin = 'failing_test_mail_collector';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearEntityData('nf_notification_preference');
    $this->clearMail();
    $this->clearQueue('nf_notification_crud');
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testFailingMailSend() {
    $group = $this->createTownSquare();
    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [NotificationOperations::CREATED],
      'town_square_types' => ['key_decisions'],
    ]);

    $this->createKeyDecision($group);

    $this->assertQueueSize(1, 'nf_notification_crud');

    // Notification mail is configured to be queued, so one out of 100 email
    // notifications could fail and 99 would be sent with 1 redelivered. Queue
    // mail will always report mail as "queued" except in truly exceptional
    // circumstances. Processing a CRUD queue will expect that generally the
    // queueing will be less error prone than the sending of emails themselves.
    // For that reason, let the CRUD queue fail with an exception to be requeued
    // even if half of the notifications for that operation may have already
    // been created. Under those rare circumstances, users may receive two
    // notifications for one operation, but not at any kind of similar frequency
    // to an external mail gateway failing.
    $this->expectExceptionMessage('Could not send or queue notification email.');
    $this->processQueue('nf_notification_crud');
  }

}

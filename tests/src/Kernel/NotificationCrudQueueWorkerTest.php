<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\document\Entity\Document;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * The NotificationCrudQueueWorkerTest class.
 */
class NotificationCrudQueueWorkerTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearEntityData('nf_notification_preference');
    $this->clearMail();
    $this->clearQueue('nf_notification_crud');
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testNotificationCrudQueueWorkerTownSquareContent() {
    $group = $this->createTownSquare('Sample group');

    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'key_decisions',
        'generic_register',
        'page',
      ],
    ]);

    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'page',
        'key_decisions',
      ],
    ]);

    // Create, update and delete the entity. One user is subscribed to all three
    // events and the other is subscribed to two.
    $key_decision = $this->createKeyDecision($group, ['title' => 'foo']);
    $key_decision->title = 'updated';
    $key_decision->save();
    $key_decision->delete();

    // 5 total emails should be sent after processing the crud queue.
    $this->assertMail()->countEquals(0);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(5);

    $this->assertMailSubjectsByIndex([
      'Sample group Key decision "foo" created',
      'Sample group Key decision "foo" created',
      'Sample group Key decision "updated" updated',
      'Sample group Key decision "updated" deleted',
      'Sample group Key decision "updated" deleted',
    ]);
  }

  /**
   * Test the queue processing once a group has been deleted.
   */
  public function testNotificationCrudQueueWorkerTownSquareContentGroupDeleted() {
    $group = $this->createTownSquare();

    $account = $this->createGroupMember($group);
    $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'key_decisions',
        'generic_register',
        'page',
      ],
    ]);

    $key_decision = $this->createKeyDecision($group);
    $key_decision->delete();
    $group->delete();

    // The queue should process successfully, but at this stage, short of trying
    // to serialize an entire group into the queue, we cannot validate any
    // users access to the content, so no queued notifications will be sent.
    // Deleting a group is not a typical activity however.
    $this->assertMail()->countEquals(0);
    $this->assertQueueSize(2, 'nf_notification_crud');
    $this->processQueue('nf_notification_crud');
    $this->assertQueueSize(0, 'nf_notification_crud');
    $this->assertMail()->countEquals(0);
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testNotificationCrudQueueWorkerOfficerContent() {
    $account = $this->createOfficer();
    $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
        NotificationOperations::COMMENTED,
      ],
      'officer_view_types' => [
        'discussion',
        'officer_page',
        'project',
      ],
    ]);

    $account = $this->createOfficer();
    $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::DELETED,
      ],
      'officer_view_types' => [
        'discussion',
      ],
    ]);

    $discussion = $this->createDiscussion(NULL, NULL, ['title' => 'sample discussion']);
    $discussion->title = 'updated';
    $discussion->save();
    $discussion->title = 'updated again';
    $discussion->save();
    $this->createDiscussionComment($this->createOfficer(), $discussion);

    $discussion->delete();

    $this->assertMail()->countEquals(0);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(8);

    $this->assertMailSubjectsByIndex([
      'Local Government Toolbox Discussion "sample discussion" created',
      'Local Government Toolbox Discussion "updated" updated',
      'Local Government Toolbox Discussion "updated" updated',
      'Local Government Toolbox Discussion "updated again" updated',
      'Local Government Toolbox Discussion "updated again" updated',
      'Local Government Toolbox Discussion "updated again" commented',
      'Local Government Toolbox Discussion "updated again" deleted',
      'Local Government Toolbox Discussion "updated again" deleted',
    ]);
  }

  /**
   * Test content is limited by profession when created in the officer view.
   */
  public function testNotificationCrudQueueWorkerOfficerProfessionMatching() {
    $preference_settings = [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
        NotificationOperations::COMMENTED,
      ],
      'officer_view_types' => [
        'discussion',
        'officer_page',
        'project',
      ],
    ];

    $foo_profession = $this->createProfession('Foo');
    $bar_profession = $this->createProfession('Bar');

    $foo_topic = $this->createOfficerTopic('Foo topic', $foo_profession);
    $bar_topic = $this->createOfficerTopic('Bar topic', $bar_profession);
    $empty_topic = $this->createOfficerTopic('Empty topic');

    $foo_officer = $this->createOfficer(NULL, $foo_profession);
    $this->createNotificationPreference($foo_officer, $preference_settings);

    $bar_officer = $this->createOfficer(NULL, $bar_profession);
    $this->createNotificationPreference($bar_officer, $preference_settings);

    $foobar_officer = $this->createOfficer();
    $foobar_officer->field_profession = [
      ['entity' => $foo_profession],
      ['entity' => $bar_profession],
    ];
    $foobar_officer->save();
    $this->createNotificationPreference($foobar_officer, $preference_settings);

    // The foo topic should notify the foo users.
    $this->createDiscussion(NULL, $foo_topic, ['title' => 'sample discussion']);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(2);
    $this->assertMail()->seekToRecipient($foo_officer->getEmail())->countEquals(1);
    $this->assertMail()->seekToRecipient($foobar_officer->getEmail())->countEquals(1);

    // The bar topic should notify the bar users.
    $this->clearMail();
    $this->createDiscussion(NULL, $bar_topic, ['title' => 'sample discussion']);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(2);
    $this->assertMail()->seekToRecipient($bar_officer->getEmail())->countEquals(1);
    $this->assertMail()->seekToRecipient($foobar_officer->getEmail())->countEquals(1);

    // Discussion with no topic should notify everyone.
    $this->clearMail();
    $this->createDiscussion(NULL, NULL, ['title' => 'sample discussion']);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(3);

    // Topics with no professions should notify everyone.
    $this->clearMail();
    $this->createDiscussion(NULL, $empty_topic, ['title' => 'sample discussion']);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(3);
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testNotificationCrudQueueWorkerScc() {
    $group = $this->createGroup('scc', 'Sunshine group');

    $account = $this->createGroupAdmin($group);
    $this->createNotificationPreference($account, [
      'type' => 'scc',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'asset_register',
        'contract',
        'contract_document',
        'scc_discussion',
        'scc_meeting',
        'incident_register',
        'supplier',
      ],
    ]);

    $entity = $this->createSupplier($group, 'Sample supplier');
    $entity->title = 'updated';
    $entity->save();
    $entity->title = 'updated again';
    $entity->save();
    $entity->delete();

    $this->assertMail()->countEquals(0);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(4);

    $this->assertMailSubjectsByIndex([
      'Sunshine group Supplier "Sample supplier" created',
      'Sunshine group Supplier "updated" updated',
      'Sunshine group Supplier "updated again" updated',
      'Sunshine group Supplier "updated again" deleted',
    ]);
  }

  /**
   * Test the crud hooks create the relevant queue items.
   */
  public function testNotificationCrudQueueWorkerToolboxTeam() {
    $group = $this->createGroup('toolbox_team', 'Toolbox team');

    $member = $this->createGroupMember($group);
    $this->createNotificationPreference($member, [
      'type' => 'toolbox_team',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'town_square_types' => [
        'key_decisions',
        'generic_register',
        'document',
        'meeting',
        'discussion',
      ],
    ]);

    $entity = $this->createGroupDiscussion($group, NULL, ['title' => 'Foodiscussion']);
    $entity->title = 'updated';
    $entity->save();
    $entity->delete();

    $this->assertMail()->countEquals(0);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(3);

    $this->assertMailSubjectsByIndex([
      'Toolbox team Discussion "Foodiscussion" created',
      'Toolbox team Discussion "updated" updated',
      'Toolbox team Discussion "updated" deleted',
    ]);
  }

  /**
   * Test the crud hooks create the relevant queue items.
   *
   * Test that a bundle that is used across multiple group types works
   * correctly.
   */
  public function testNotificationCrudQueueWorkerTownSquareDuplicateBundle() {
    $team_group = $this->createGroup('toolbox_team', 'Toolbox team');
    $town_square_group = $this->createTownSquare('Town square');

    $team_member = $this->createGroupMember($team_group);
    $town_square_member = $this->createGroupMember($town_square_group);

    $team_preference = $this->createNotificationPreference($team_member, [
      'type' => 'toolbox_team',
      'town_square_group' => $team_group->id(),
      'operations' => [NotificationOperations::CREATED],
      'town_square_types' => ['discussion'],
    ]);
    $square_preference = $this->createNotificationPreference($town_square_member, [
      'type' => 'town_square',
      'town_square_group' => $town_square_group->id(),
      'operations' => [NotificationOperations::CREATED],
      'town_square_types' => ['discussion'],
    ]);

    $team_discussion = $this->createGroupDiscussion($team_group, NULL, ['title' => 'Team discussion']);
    $town_square = $this->createGroupDiscussion($town_square_group, NULL, ['title' => 'Town square discussion']);

    $this->assertMail()->countEquals(0);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(2);

    $this->assertMailSubjectsByIndex([
      'Toolbox team Discussion "Team discussion" created',
      'Town square Discussion "Town square discussion" created',
    ]);
  }

  /**
   * Tests orphan notifications process in the queue fine.
   */
  public function testNotificationCrudQueueWorkerOwnerDeleted() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [NotificationOperations::CREATED],
      'officer_view_types' => ['discussion'],
    ]);

    // Ensure the preference intentionally belongs to an orphan account, so the
    // queue can be tested against faulty data.
    $preference->setOwnerId(91919191);
    $preference->save();
    $account->delete();

    $this->createDiscussion(NULL, NULL, ['title' => 'sample discussion']);

    $this->assertMail()->countEquals(0);
    $this->assertQueueSize(1, 'nf_notification_crud');
    $this->processQueue('nf_notification_crud');
    $this->assertQueueSize(0, 'nf_notification_crud');
    $this->assertMail()->countEquals(0);
  }

  /**
   * Test document notifications.
   */
  public function testNotificationCrudQueueWorkerDocuments() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      // Only notify for documents that are designated public.
      'custom_access' => [
        Document::ACCESS_PUBLIC,
      ],
    ]);

    $document = $this->createDocument(NULL, NULL, NULL, [
      'custom_access' => Document::ACCESS_PUBLIC,
    ]);
    $non_notify_document = $this->createDocument(NULL, NULL, NULL, [
      'custom_access' => Document::ACCESS_OFFICERS_ONLY,
    ]);

    $this->assertQueueSize(2, 'nf_notification_crud');
    $this->processQueue('nf_notification_crud');
    $this->assertQueueSize(0, 'nf_notification_crud');

    $document->label = 'updated';
    $document->save();

    $this->assertQueueSize(1, 'nf_notification_crud');
    $this->processQueue('nf_notification_crud');
    $this->assertQueueSize(0, 'nf_notification_crud');

    $document->delete();

    $this->assertQueueSize(1, 'nf_notification_crud');
    $this->processQueue('nf_notification_crud');
    $this->assertQueueSize(0, 'nf_notification_crud');

    $this->assertMail()->countEquals(2);
  }

  /**
   * Test the profession matching of documents.
   */
  public function testNotificationCrudQueueWorkerDocumentProfessionMatching() {
    $preference_settings = [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
      ],
    ];

    $foo_profession = $this->createProfession('Foo');
    $bar_profession = $this->createProfession('Bar');

    $foo_topic = $this->createOfficerTopic('Foo topic', $foo_profession);
    $bar_topic = $this->createOfficerTopic('Bar topic', $bar_profession);
    $empty_topic = $this->createOfficerTopic('Empty topic');

    $foo_officer = $this->createOfficer(NULL, $foo_profession);
    $this->createNotificationPreference($foo_officer, $preference_settings);

    $bar_officer = $this->createOfficer(NULL, $bar_profession);
    $this->createNotificationPreference($bar_officer, $preference_settings);

    $foobar_officer = $this->createOfficer();
    $foobar_officer->field_profession = [
      ['entity' => $foo_profession],
      ['entity' => $bar_profession],
    ];
    $foobar_officer->save();
    $this->createNotificationPreference($foobar_officer, $preference_settings);

    // The foo topic should notify the foo users.
    $this->createDocument(NULL, NULL, NULL, [
      'topic' => ['entity' => $foo_topic],
    ]);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(2);
    $this->assertMail()->seekToRecipient($foo_officer->getEmail())->countEquals(1);
    $this->assertMail()->seekToRecipient($foobar_officer->getEmail())->countEquals(1);

    // The bar topic should notify the bar users.
    $this->clearMail();
    $this->createDocument(NULL, NULL, NULL, [
      'topic' => ['entity' => $bar_topic],
    ]);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(2);
    $this->assertMail()->seekToRecipient($bar_officer->getEmail())->countEquals(1);
    $this->assertMail()->seekToRecipient($foobar_officer->getEmail())->countEquals(1);

    // Discussion with no topic should notify everyone.
    $this->clearMail();
    $this->createDocument(NULL, NULL, NULL, [
      'topic' => NULL,
    ]);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(3);

    // Topics with no professions should notify everyone.
    $this->clearMail();
    $this->createDocument(NULL, NULL, NULL, [
      'topic' => ['entity' => $empty_topic],
    ]);
    $this->processQueue('nf_notification_crud');
    $this->assertMail()->countEquals(3);
  }

}

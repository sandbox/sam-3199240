<?php

namespace Drupal\Tests\notification_framework\Functional;


/**
 * Test the preferences entity access.
 *
 * @coversDefaultClass \Drupal\notification_framework\NotificationPreferenceEntityAccessControlHandler
 */
class PreferenceAccessTest extends NotificationFrameworkKernelTestBase {

  /**
   * Test create access.
   */
  public function testTownSquareCreateAccess() {
    $handler = $this->container->get('entity_type.manager')->getAccessControlHandler('nf_notification_preference');

    // A user not a member of any groups cannot create town square
    // notifications.
    $account = $this->createUser();
    $this->assertFalse($handler->createAccess('town_square', $account));

    // Admins can create all types of notifications.
    $admin = $this->createToolboxAdmin();
    $this->assertTrue($handler->createAccess('town_square', $admin));

    // Users a member of non-town square groups, cannot create town square
    // notifications.
    $group = $this->createGroup('scc');
    $account = $this->createGroupMember($group);
    $this->assertFalse($handler->createAccess('town_square', $account));

    // Users a member of non-town square groups, cannot create town square
    // notifications.
    $group = $this->createGroup('town_square');
    $account = $this->createGroupMember($group);
    $this->assertTrue($handler->createAccess('town_square', $account));
  }

  /**
   * Test the town square access when it's overridden by the route.
   */
  public function testTownSquareCreateAccessProfileOverride() {
    $handler = $this->container->get('entity_type.manager')->getAccessControlHandler('nf_notification_preference');

    $admin = $this->createToolboxAdmin();
    $account = $this->createUser();

    // An admin viewing a standard member should have the create access of the
    // standard member and should not be able to create town square
    // notifications.
    $this->setCurrentRouteMatch('nf_notification_crud_ui.notifications', ['user' => $account]);
    $this->assertFalse($handler->createAccess('town_square', $admin));
  }

  /**
   * Test the officer view create access.
   */
  public function testOfficerViewCreateAccess() {
    $handler = $this->container->get('entity_type.manager')->getAccessControlHandler('nf_notification_preference');

    $officer = $this->createOfficer();
    $this->assertTrue($handler->createAccess('officer_view', $officer));

    $user = $this->createUser();
    $this->assertFalse($handler->createAccess('officer_view', $user));

    $admin = $this->createToolboxAdmin();
    $this->assertTrue($handler->createAccess('officer_view', $admin));
  }

  /**
   * Test the officer view access when it's overriden by the route.
   */
  public function testOfficerViewCreateAccessProfileOverride() {
    $admin = $this->createToolboxAdmin();
    $user = $this->createUser();
    $this->setCurrentRouteMatch('notification_framework_ui.notifications', ['user' => $user]);
    $handler = $this->container->get('entity_type.manager')->getAccessControlHandler('nf_notification_preference');
    $this->assertFalse($handler->createAccess('officer_view', $admin));
  }

  /**
   * Test edit access to notification preferences.
   */
  public function testNotificationAccess() {
    $handler = $this->container->get('entity_type.manager')->getAccessControlHandler('nf_notification_preference');

    $owner_account = $this->createUser();
    $non_owner_account = $this->createUser();
    $admin = $this->createToolboxAdmin();
    $preference = $this->createNotificationPreference($owner_account, [
      'type' => 'town_square',
    ]);

    $this->assertTrue($handler->access($preference, 'update', $owner_account));
    $this->assertTrue($handler->access($preference, 'update', $admin));
    $this->assertFalse($handler->access($preference, 'update', $non_owner_account));

    $this->assertTrue($handler->access($preference, 'view', $owner_account));
    $this->assertTrue($handler->access($preference, 'view', $admin));
    $this->assertFalse($handler->access($preference, 'view', $non_owner_account));

    $this->assertTrue($handler->access($preference, 'delete', $owner_account));
    $this->assertTrue($handler->access($preference, 'delete', $admin));
    $this->assertFalse($handler->access($preference, 'delete', $non_owner_account));
  }

}

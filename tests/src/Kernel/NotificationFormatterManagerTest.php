<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatter\SingleOfficerView;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatter\SingleTownSquare;

/**
 * @coversDefaultClass \Drupal\notification_framework\PluginManager\NotificationFormatterManager
 */
class NotificationFormatterManagerTest extends NotificationFrameworkKernelTestBase {

  /**
   * The type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationFormatterManager
   */
  protected $formatManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->formatManager = $this->container->get('plugin.manager.notification_framework_formatter');
  }

  /**
   * @covers ::createInstanceForNotifications
   */
  public function testCreateInstanceForNotifications() {
    $this->assertNull($this->formatManager->createInstanceForNotifications([]));

    $town_square_notification = $this->createNotification([
      'notification_preference_type' => 'town_square',
    ]);
    $this->assertInstanceOf(SingleTownSquare::class, $this->formatManager->createInstanceForNotifications([$town_square_notification]));

    $officer_view_notification = $this->createNotification([
      'notification_preference_type' => 'officer_view',
    ]);
    $this->assertInstanceOf(SingleOfficerView::class, $this->formatManager->createInstanceForNotifications([$officer_view_notification]));
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Entity\Notification;
use Drupal\notification_framework\Entity\NotificationSummary;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * Test the notification preference entity.
 *
 * @coversDefaultClass \Drupal\notification_framework\Entity\Notification
 */
class NotificationEntityTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * @covers ::createFrom
   */
  public function testCreateFromTownSquareContent() {
    $group = $this->createGroup('town_square');
    $user = $this->createGroupMember($group);
    $entity = $this->createKeyDecision($group);
    $preference = $this->createNotificationPreference($user, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'key_decisions',
      ],
    ]);
    $summary = NotificationSummary::createFrom($entity, NotificationOperations::UPDATED);
    $summary->save();
    $this->markEntityForCleanup($summary);

    $notification = Notification::createFrom($summary, $preference);
    $this->markEntityForCleanup($notification);
    $notification->save();

    $this->assertNotNull($notification->created->value);
    $this->assertEquals($preference->id(), $notification->notification_preference->target_id);
    $this->assertFalse($notification->email_sent->value);
    $this->assertTrue($notification->email_sent_time->isEmpty());

    $this->assertMail()->countEquals(0);
    $this->assertTrue($notification->send());
    $this->assertMail()->countEquals(1);

    $this->assertTrue($notification->email_sent->value);
    $this->assertFalse($notification->email_sent_time->isEmpty());

    // Ensure notifications can be replayed, even once their associated
    // preferences have been deleted.
    $preference->delete();
    $this->assertTrue($notification->send());
    $this->assertMail()->countEquals(2);
  }

}

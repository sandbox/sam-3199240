<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Entity\NotificationSummary;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * Test the notification summary entity.
 *
 * @coversDefaultClass \Drupal\notification_framework\Entity\NotificationSummary
 */
class NotificationSummaryEntityTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * @covers ::createFrom
   */
  public function testNotificationSummaryEntity() {
    $group = $this->createGroup('town_square');
    $entity = $this->createGroupDiscussion($group, NULL, [
      'field_body' => [
        'value' => '<p>test</p>',
        'format' => 'full_html',
      ],
    ]);

    $summary = NotificationSummary::createFrom($entity, NotificationOperations::UPDATED);
    $summary->attachments = [
      ['foo' => 'bar'],
    ];
    $this->markEntityForCleanup($summary);
    $summary->save();

    $this->assertNotNull($summary->created->value);
    $this->assertEquals('town_square_content', $summary->notify_entity_type_id->value);
    $this->assertEquals([['foo' => 'bar']], $summary->getAttachments());
    $this->assertEquals($entity->id(), $summary->notify_entity_id->value);
    $this->assertEquals($entity->getRevisionId(), $summary->notify_entity_revision_id->value);
    $this->assertEquals($entity->label(), $summary->notify_entity_label->value);
    $this->assertEquals(NotificationOperations::UPDATED, $summary->operation->value);
    $this->assertEquals($group->id(), $summary->type_data->group);
    $this->assertContains('<p>test</p>', $summary->getChangeSummary());

    $this->assertEquals(NotificationOperations::UPDATED, $summary->getOperation());
    $this->assertEquals($entity->label(), $summary->getNotifyLabel());
    $this->assertEquals($entity->bundle(), $summary->getNotifyBundle());
    $this->assertEquals($entity->id(), $summary->getNotifyEntity()->id());
  }

}

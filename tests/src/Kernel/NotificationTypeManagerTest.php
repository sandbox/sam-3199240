<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkType\OfficerViewNotification;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkType\TownSquareNotification;

/**
 * @coversDefaultClass \Drupal\notification_framework\PluginManager\NotificationTypeManager
 */
class NotificationTypeManagerTest extends NotificationFrameworkKernelTestBase {

  /**
   * The type manager.
   *
   * @var \Drupal\notification_framework\PluginManager\NotificationTypeManager
   */
  protected $typeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->typeManager = $this->container->get('plugin.manager.notification_framework_type');
  }

  /**
   * @covers ::createInstanceForEntity
   */
  public function testCreateInstanceForEntity() {
    $this->assertNull($this->typeManager->createInstanceForEntity($this->createProfession()));

    $this->assertInstanceOf(OfficerViewNotification::class, $this->typeManager->createInstanceForEntity($this->createDiscussion()));
    $this->assertNull($this->typeManager->createInstanceForEntity($this->createCouncilPage()));

    $group = $this->createGroup('town_square');
    $this->assertInstanceOf(TownSquareNotification::class, $this->typeManager->createInstanceForEntity($this->createKeyDecision($group)));
  }

}

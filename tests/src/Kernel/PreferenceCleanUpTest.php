<?php

namespace Drupal\notification_framework\Tests\Functional;

use Drupal\notification_framework\Constant\NotificationOperations;

/**
 * Test preferences are cleaned up when users are deleted.
 */
class PreferenceCleanUpTest extends NotificationFrameworkKernelTestBase {

  /**
   * Test preferences are cleaned up when users are deleted.
   */
  public function testPreferenceCleanUp() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [NotificationOperations::UPDATED],
      'officer_view_types' => ['discussion'],
    ]);

    $account->delete();
    $this->assertNull($this->reloadEntity($preference));
  }

}

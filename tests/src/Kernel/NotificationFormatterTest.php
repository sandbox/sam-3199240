<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Entity\Notification;
use Drupal\notification_framework\Entity\NotificationSummary;
use Drupal\notification_framework\Constant\NotificationOperations;
use Drupal\Tests\notification_framework\AdvancedMailAssertTrait;

/**
 * Test the format of notification emails.
 */
class NotificationFormatterTest extends NotificationFrameworkKernelTestBase {

  use AdvancedMailAssertTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->clearMail();
  }

  /**
   * Test the format of notification emails.
   */
  public function testTownSquareNotificationFormat() {
    $group = $this->createTownSquare('Foogroup');
    $account = $this->createGroupMember($group);
    $content = $this->createGenericRegister($group, 'Sample register');
    $preference = $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::UPDATED,
      ],
      'town_square_types' => [
        'page',
        'generic_register',
      ],
    ]);

    $summary = NotificationSummary::createFrom($content, NotificationOperations::UPDATED);
    $summary->save();

    $notification = Notification::createFrom($summary, $preference);
    $notification->save();

    // Ensure neither the preference of content need to still exist for
    // notifications to format correctly.
    $preference->delete();
    $content->delete();

    $notification->send();
    $this->assertMail()
      ->subjectEquals('Foogroup Generic register "Sample register" updated')
      ->bodyContains('*Group name:* Foogroup')
      ->bodyContains('*Content type:* Generic register')
      ->bodyContains('*Content operation:* Updated');
  }

  /**
   * Test the format of notification emails.
   */
  public function testOfficerViewNotificationFormat() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [
        NotificationOperations::UPDATED,
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
      'officer_view_types' => [
        'discussion',
        'officer_page',
        'project',
      ],
    ]);

    $discussion = $this->createDiscussion(NULL, NULL, [
      'title' => 'Foodiscussion',
    ]);

    $summary = NotificationSummary::createFrom($discussion, NotificationOperations::UPDATED);
    $summary->save();

    $notification = Notification::createFrom($summary, $preference);
    $notification->save();

    // Ensure neither the preference of content need to still exist for
    // notifications to format correctly.
    $preference->delete();
    $discussion->delete();

    $notification->send();
    $this->assertMail()
      ->subjectEquals('Local Government Toolbox Discussion "Foodiscussion" updated')
      ->bodyContains('FOODISCUSSION')
      ->bodyContains('*Content operation:* Updated')
      ->bodyContains('*Content type:* Discussion');
  }

  /**
   * Test the format of notification emails.
   */
  public function testSccNotificationFormat() {
    $group = $this->createGroup('scc', 'SCC Group');

    $account = $this->createGroupAdmin($group);
    $preference = $this->createNotificationPreference($account, [
      'type' => 'scc',
      'town_square_group' => $group->id(),
      'operations' => [NotificationOperations::UPDATED],
      'town_square_types' => ['supplier'],
    ]);
    $entity = $this->createSupplier($group, 'Sample supplier');

    $summary = NotificationSummary::createFrom($entity, NotificationOperations::UPDATED);
    $summary->save();

    $notification = Notification::createFrom($summary, $preference);
    $notification->save();

    // Ensure neither the preference of content need to still exist for
    // notifications to format correctly.
    $preference->delete();
    $entity->delete();

    $notification->send();
    $this->assertMail()
      ->subjectEquals('SCC Group Supplier "Sample supplier" updated')
      ->bodyContains('SAMPLE SUPPLIER')
      ->bodyContains('*Content operation:* Updated')
      ->bodyContains('*Group name:* SCC Group')
      ->bodyContains('*Content type:* Supplier');
  }

  /**
   * Test the officer notification format with links.
   */
  public function testOfficerNotificationFormatWithLinks() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'officer_view',
      'operations' => [NotificationOperations::UPDATED],
      'officer_view_types' => ['discussion'],
    ]);
    $discussion = $this->createDiscussion(NULL, NULL, [
      'title' => 'Foodiscussion',
    ]);

    $summary = NotificationSummary::createFrom($discussion, NotificationOperations::UPDATED);
    $summary->save();

    $notification = Notification::createFrom($summary, $preference);
    $notification->save();
    $notification->send();
    $this->assertMail()->bodyContains($discussion->toUrl()->toString());
  }

  /**
   * Test the town square notification with links.
   */
  public function testTownSquareFormatWithLinks() {
    $group = $this->createTownSquare('Foogroup');
    $account = $this->createGroupMember($group);
    $content = $this->createGenericRegister($group, 'Sample register');
    $preference = $this->createNotificationPreference($account, [
      'type' => 'town_square',
      'town_square_group' => $group->id(),
      'operations' => [NotificationOperations::UPDATED],
      'town_square_types' => ['generic_register'],
    ]);

    $summary = NotificationSummary::createFrom($content, NotificationOperations::UPDATED);
    $summary->save();
    $notification = Notification::createFrom($summary, $preference);
    $notification->save();
    $notification->send();
    $this->assertMail()->bodyContains($content->toUrl()->toString());
  }

  /**
   * Test the format of the single document notification emails.
   */
  public function testSingleDocumentFormat() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
    ]);
    $document = $this->createDocument(NULL, NULL, NULL, [
      'name' => 'Sample doc',
    ]);

    $summary = NotificationSummary::createFrom($document, NotificationOperations::CREATED);
    $summary->save();
    $notification = Notification::createFrom($summary, $preference);
    $notification->save();

    $document->delete();
    $notification->send();

    $this->assertMail()
      ->countEquals(1)
      ->bodyContains('*Content operation:* Created')
      ->subjectEquals('Document "Sample doc" created');
  }

  /**
   * Test the format of the single document notification emails with links.
   */
  public function testSingleDocumentFormatWithLinks() {
    $account = $this->createOfficer();
    $preference = $this->createNotificationPreference($account, [
      'type' => 'document',
      'operations' => [
        NotificationOperations::CREATED,
        NotificationOperations::DELETED,
      ],
    ]);
    $document = $this->createDocument(NULL, NULL, NULL, [
      'name' => 'Sample doc',
    ]);

    $summary = NotificationSummary::createFrom($document, NotificationOperations::CREATED);
    $summary->save();
    $notification = Notification::createFrom($summary, $preference);
    $notification->save();
    $notification->send();

    $this->assertMail()
      ->countEquals(1)
      ->bodyContains('*Content operation:* Created')
      ->subjectEquals('Document "Sample doc" created')
      ->bodyContains('/officer/documents/' . $document->id());
  }

}

<?php

namespace Drupal\Tests\notification_framework\Functional;

use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\user\Entity\User;

/**
 * Test the notification entity.
 */
class PreferenceEntityTest extends NotificationFrameworkKernelTestBase {

  /**
   * Test the notification plugins are found.
   */
  public function testNotificationPreferenceEntityBundles() {
    $group = $this->createGroup('town_square');
    $preference = NotificationPreference::create([
      'type' => 'town_square',
      'town_square_group' => ['entity' => $group],
      'owner' => 123,
    ]);
    $this->assertEqual($group->id(), $preference->town_square_group->target_id);
  }

  /**
   * Test the entity owner exception.
   */
  public function testNotificationPreferenceEntityOwnerException() {
    $this->expectExceptionMessage('The owner ID must be provided when creating a notification preference or the preference must be authored on the user notification tab.');
    NotificationPreference::create([
      'type' => 'town_square',
    ]);
  }

  /**
   * Test the entity owner is derived from the route.
   *
   * Since admins may navigate to any user and create notifications on their
   * behalf, the owner of the notification isn't the logged in user, but the
   * user profile being viewed.
   */
  public function testNotificationPreferenceEntityOwnerRoute() {
    $account = $this->prophesize(User::class);
    $account->id()->willReturn(123);
    $this->setCurrentRouteMatch('notification_framework_ui.notifications', ['user' => $account->reveal()]);
    $preference = NotificationPreference::create([
      'type' => 'town_square',
    ]);
    $this->assertEqual(123, $preference->owner->target_id);
  }

  /**
   * Test the entity owner is derived from the route.
   */
  public function testNotificationPreferenceNonEntityOwnerRoute() {
    $this->expectExceptionMessage('The owner ID must be provided when creating a notification preference or the preference must be authored on the user notification tab.');
    $this->setCurrentRouteMatch('system.404');
    NotificationPreference::create([
      'type' => 'town_square',
    ]);
  }

}

<?php

namespace Drupal\notification_framework_test\Plugin\NotificationFramework\NotificationFrameworkFormatter;

use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkFormatterBase;
use Drupal\notification_framework_test\Plugin\NotificationFramework\NotificationFrameworkType\SampleNodeNotification;

/**
 * Single town square notification email.
 *
 * @NotificationFrameworkFormatter(
 *   id = "single_sample_node_formatter",
 *   label = @Translation("Single sample node formatter"),
 * )
 */
class SingleSampleNodeFormatter extends NotificationFrameworkFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function getSubject(array $notifications): string {
    /** @var \Drupal\notification_framework\Entity\Notification $notification */
    $notification = array_shift($notifications);
    return sprintf('Sample site %s "%s" %s', $notification->getSummary()->getNotifyBundle(), $notification->getSummary()->getNotifyLabel(), $notification->getSummary()->getOperation());
  }

  /**
   * {@inheritdoc}
   */
  public function getBody(array $notifications): array {
    /** @var \Drupal\notification_framework\Entity\Notification $notification */
    $notification = current($notifications);

    $build = parent::getBody($notifications);
    $build += [
      '#theme' => 'notification_framework_single_entity',
      '#entity_label' => $notification->getSummary()->getNotifyLabel(),
      '#change_summary' => $notification->getSummary()->getChangeSummary(),
      '#entity_bundle' => $notification->getSummary()->getNotifyBundle(),
      '#notify_entity_exists' => $notification->getSummary()->getNotifyEntity() !== NULL,
      '#notify_entity' => $notification->getSummary()->getNotifyEntity(),
      '#operation' => $notification->getSummary()->getOperationLabel(),
      '#account' => $notification->getOwner(),
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(array $notifications): bool {
    if (count($notifications) !== 1) {
      return FALSE;
    }
    /** @var \Drupal\notification_framework\Entity\Notification $notification */
    $notification = array_shift($notifications);
    return $notification->getNotificationType() instanceof SampleNodeNotification;
  }

}

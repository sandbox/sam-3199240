<?php

namespace Drupal\notification_framework_test\Plugin\NotificationFramework\NotificationFrameworkType;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity\BundleFieldDefinition;
use Drupal\notification_framework\Entity\NotificationPreference;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkTypeBase;

/**
 * Officer view notifications.
 *
 * @NotificationFrameworkType(
 *   id = "sample_node",
 *   label = @Translation("Sample node"),
 * )
 */
class SampleNodeNotification extends NotificationFrameworkTypeBase {

  /**
   * A list of bundles to support for this plugin.
   */
  const CONTENT_TYPES = [
    'article' => 'Article',
    'page' => 'Basic page',
  ];

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields['node_types'] = BundleFieldDefinition::create('list_string')
      ->setLabel(t('Content types'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => static::CONTENT_TYPES,
      ])
      ->setDefaultValue(array_keys(static::CONTENT_TYPES))
      ->setDisplayOptions('form', [
        'type' => 'options_buttons',
        'weight' => 2,
        'settings' => [],
      ]);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function summarizeNotificationSettings(NotificationPreference $entity): string {
    if ($entity->node_types->count() === count(static::CONTENT_TYPES)) {
      return $this->t('All content types');
    }
    $labels = [];
    foreach ($entity->node_types as $type) {
      $labels[] = static::CONTENT_TYPES[$type->value];
    }
    return implode($labels, ', ');
  }

  /**
   * {@inheritdoc}
   */
  public function checkCreateAccess(AccountInterface $account): AccessResultInterface {
    return AccessResult::allowedIfHasPermission($account, 'nf subscribe to notifications');
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity): bool {
    return $entity->getEntityTypeId() === 'node' && in_array($entity->bundle(), array_keys(static::CONTENT_TYPES), TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function buildApplicableNotificationPreferencesEntityQuery(QueryInterface $query, EntityInterface $entity, string $op): void {
    $query->condition('node_types', $entity->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function shouldNotify(EntityInterface $content, string $op, NotificationPreference $notificationPreference): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getNotificationMetadata(EntityInterface $notifyEntity, string $operation): array {
    return [];
  }

}

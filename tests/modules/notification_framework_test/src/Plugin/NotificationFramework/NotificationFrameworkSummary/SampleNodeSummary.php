<?php

namespace Drupal\notification_framework_test\Plugin\NotificationFramework\NotificationFrameworkSummary;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\notification_framework\Plugin\NotificationFramework\NotificationFrameworkSummaryBase;

/**
 * Notification summary plugin.
 *
 * @NotificationFrameworkSummary(
 *   id = "sample_node_summary",
 *   label = @Translation("Sample node summary"),
 * )
 */
class SampleNodeSummary extends NotificationFrameworkSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function getSummary(EntityInterface $entity, string $operation): FormattableMarkup {
    $body_build = $entity->body->view(['label' => 'hidden']);
    return new FormattableMarkup(
      '<h3>Node summary:</h3>
      <ul>
        <li><strong>Title:</strong> @title</li>
        <li><strong>Body:</strong> @body</li>
      </ul>',
      [
        '@title' => $entity->label(),
        '@body' => $this->renderer->renderPlain($body_build),
      ],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isApplicable(EntityInterface $entity, string $operation): bool {
    return $entity->getEntityTypeId() === 'node';
  }

}
